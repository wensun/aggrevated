import numpy as np
from IPython import embed
import gym
#from policy_discrete import Policy_discrete
#from RNN_policy import LSTM_Policy
from AggreVate import AggreVate_set
from parser import *
import copy
from scipy import stats
import sys
from parser import *
import cPickle


class AggreVate_parsing(AggreVate_set):
    '''
    This is designed for dependency parsing task. 
    '''
    def __init__(self, env, policy_model, expert, feature_selector, 
                    obs_dim, x_dim, a_dim, num_actions, 
                    T = 100, num_roll_in = 10,
                    imitation_train_method = 'explicit GD L2', 
                    policy_train_method = 'explicit GD L2', 
                    setup_filter = False, image = False, params = None, vr = True, testing = False,
                    im_reinforce = True):
        
        AggreVate_set.__init__(self, env, policy_model, 
                            obs_dim, x_dim, a_dim, T, 
                            num_roll_in, imitation_train_method, 
                            policy_train_method, setup_filter, vr = vr);
        self.num_actions = num_actions;
        assert self.num_actions == len(self.env.transitions)
        self.expert = expert;
        self.fea_selector = feature_selector;
        self.alg_params = params;
        self.testing = testing;
        self.im_reinforce = im_reinforce;

    def pi_star(self, cnfg):
        if self.expert.goldarcs is None:
            print "optimal expert needs goldArcs"
            assert False;
        action = self.expert.sample_action(cnfg);
        return [action,1.0];
        
    def pi(self, obs, cnfg = None):
        if cnfg is None:
            [action, prob] = self.policy.sample_action(obs);
        else:
            self.policy.update_hidden_cell(obs);
            prob_vec = self.policy.predict_prob(self.policy.hidden,
                                    self.policy.cell_mem);
            for i in xrange(prob_vec.shape[0]):
                if self.env.legal_action(i, cnfg) is False: #if the action is illegal.
                    prob_vec[i] = 0.0;
            
            prob_vec = prob_vec / (np.sum(prob_vec) + 1e-7);
            xk = np.arange(prob_vec.shape[0]);
            custm = stats.rv_discrete(name='custm', values=(xk, prob_vec));
            action = custm.rvs();
            prob = prob_vec[action];
        return [action, prob];

    def get_state(self):
        return self.env.cnfg; #return the struct configuration. 

    def set_state(self, cnfg):
        self.env.cnfg = copy.deepcopy(cnfg);
    
    def get_obs(self):
        return self.fea_selector.select(self.env.cnfg);
    
    def optimal_ctg(self, cnfg, action, goldArcs):
        self.set_state(cnfg); #set the env configuration to cnfg.
        act = action; #apply the given action. 
        while True:
            obs,done = self.env.step(act);
            if done is True:
                break;
            act = self.pi_star(self.get_state())[0]; #call expert. 
        recall = compute_recall(self.get_state(), goldArcs);
        self.set_state(cnfg); #set it back.
        return -recall;
    
    def ctg_star_oracle(self, cnfg, goldArcs):
        q_star = np.zeros(len(self.env.transitions));
        for i in xrange(q_star.shape[0]):
            q_star[i] = self.optimal_ctg(cnfg, i, goldArcs);
        if self.vr is True:
            q_star = q_star - np.min(q_star); #compute advantage 
        return q_star; #vector of negative recalls.

    def rolling_in(self, featureTSs, parses, expert = False, collect_optimal_ctg = True, prob_exp = 0.0):
        trajs_obs = [];
        trajs_state = [];
        trajs_a = [];
        trajs_exp_ctg = [];
        trajs_curr_ctg = [];
        trajs_cc = [];
        trajs_prob = [];

        for i in xrange(len(featureTSs)): 
            self.policy.reset(featureTSs[i]);#reset for RNN policy.
            obs = self.env.reset(featureTSs[i]);
            #print obs.shape[0]
            self.expert.reset(parses[i]); #the expert depends on the corresponding parse..
            self.expert.getGoldArcs();

            if obs.ndim == 1:
                traj_obs = np.zeros((self.T, obs.shape[0]));
            elif obs.ndim == 3:
                traj_obs = np.zeros((self.T,obs.shape[0],obs.shape[1],obs.shape[2]));
            
            traj_a = np.ones(self.T)*(-np.inf);
            traj_prob = np.zeros(self.T);
            traj_state = [None] * self.T;
            traj_len = 0;
            traj_curr_ctg = np.ones(self.T);
            for t in xrange(self.T):
                #print t
                if expert is True:
                    [act,prob_a] = self.pi_star(self.get_state());
                else:
                    #p_exp = 1;
                    #[act,prob_a] = self.pi_star(self.get_state()) if p_exp <= prob_exp else self.pi(obs,self.get_state());
                    [act,prob_a] = self.pi(obs,self.get_state());

                traj_obs[t] = obs;
                traj_a[t] = act;
                traj_state[t] = self.get_state();
                traj_prob[t] = prob_a;
                traj_len = traj_len + 1;
                #print act
                obs,done = self.env.step(act);
                if done:
                    break;
            traj_true_c = -compute_recall(self.get_state(), self.expert.goldarcs); #recall is reward. 
            traj_curr_ctg = traj_curr_ctg * traj_true_c;
            #print "at traj {}, the true cost is {}".format(i, traj_true_c);
            if collect_optimal_ctg is True:
                traj_ctg = np.ones((self.T, self.num_actions))*(-np.inf);
                for step in xrange(traj_len):
                    traj_ctg[step] = self.ctg_star_oracle(
                        traj_state[step], self.expert.goldarcs);
                trajs_exp_ctg.append(traj_ctg);
            
            trajs_obs.append(traj_obs);
            trajs_state.append(traj_state);
            trajs_a.append(traj_a);
            trajs_cc.append(traj_true_c);
            trajs_curr_ctg.append(traj_curr_ctg);
            trajs_prob.append(traj_prob);
        
        return {'list_obs':trajs_obs, 'list_state':trajs_state, 'list_U':trajs_a, 
                'list_tc':trajs_cc, 'list_u_probs':trajs_prob, 
                'list_exp_ctg':trajs_exp_ctg, 'list_curr_ctg':trajs_curr_ctg};

    def initialize_policy(self, featureTSs, pad_featureTSs,parses, n_epch = 32, batch_size = 20, lr = 0.001, nll = True):
        N_train = len(featureTSs);
        assert N_train == len(parses);
        #if self.testing is True:
        inputs_dic = self.rolling_in(featureTSs, parses, expert = True, collect_optimal_ctg = True);
        list_obs = inputs_dic['list_obs'];
        list_exp_ctg = inputs_dic['list_exp_ctg'];
        list_u = inputs_dic['list_U'];
        
        #else:
        #    if nll is False:
        #        [list_obs,list_exp_ctg] = cPickle.load(open('pre_computed_input_dic_for_testing.save','rb'));
        #        list_obs = list_obs[0:320];
        #        list_exp_ctg = list_exp_ctg[0:320];
        #    else:
        #        [list_obs,list_u] = cPickle.load(open('pre_computed_input_dic_nll_for_testing_{}.save'.format(self.alg_params['system']),'rb'));
        #        list_obs = list_obs[0:320];
        #        list_u = list_u[0:320];

        for e in xrange(n_epch):
            #print e
            b_start = 0;
            while b_start < N_train:
                b_end = np.min([b_start+batch_size, N_train]);
                if nll is False:
                    output = self.policy.fit_im_cost_sensitive(X1_list = pad_featureTSs[b_start:b_end],
                            X2_list = list_obs[b_start:b_end],
                            Y_list = list_exp_ctg[b_start:b_end], lr = lr);
                elif nll is True:
                    output = self.policy.train_nll(pad_featureTSs[b_start:b_end], 
                            list_obs[b_start:b_end], list_u[b_start:b_end], lr);

                b_start = b_start+batch_size;
                #print output
    
    def test_policy(self, featureTSs, parses, expert = False):
        inputs_dic = self.rolling_in(featureTSs, parses, expert = expert,
                    collect_optimal_ctg = False, prob_exp = 0.0);
        return [np.mean(inputs_dic['list_tc']), np.std(inputs_dic['list_tc'])];

    def AggreVate_imitation(self, featureTSs_train, parses_train, 
                    featureTSs_valid, parses_valid,
                    total_iter = 100, batch_size = 20, 
                    pre_train = True, n_epoch = 100, lr = 1e-1, KL_Delta = 0.1, alpha = 0.1):
        #batch_size is equivalent to the number of rolling-in.
        len_max = np.max([a.shape[0] for a in featureTSs_train]);
        pad_featureTSs_train = [None]*len(featureTSs_train);
        for traj_i in xrange(len(featureTSs_train)):
            pad_featureTSs_train[traj_i] = np.ones((len_max,featureTSs_train[traj_i].shape[1]))*(-np.inf);
            pad_featureTSs_train[traj_i][0:featureTSs_train[traj_i].shape[0]] = featureTSs_train[traj_i];

        original_KL_Delta = KL_Delta;
        test_per_per_period = [];
        N_train = len(featureTSs_train);
        if pre_train:
            print "##########Initialization###############";
            initial_rr = self.test_policy(featureTSs_valid,parses_valid,expert=False);
            test_per_per_period.append(initial_rr);
            print "[im] Before initialization: mean {} and std {}".format(
                                initial_rr[0],initial_rr[1]);
            if self.alg_params['system'] == 'hybird':
                self.initialize_policy(featureTSs_train,pad_featureTSs_train, parses_train, n_epch = n_epoch,
                    batch_size = 50, lr = 1e-2); #1e-4 works well for hybird. 1e-3 for eager
            else:
                self.initialize_policy(featureTSs_train,pad_featureTSs_train, parses_train, n_epch = n_epoch,
                    batch_size = 50, lr = 1e-2); #1e-4 works well for hybird. 1e-3 for eager
        
        print "start AggreVate....";
        for iteration in xrange(total_iter):
            p_expert = (1.0 - alpha)**(iteration+1);

            if iteration <= total_iter / 2.:
                KL_Delta = original_KL_Delta;
            else:
                KL_Delta = original_KL_Delta/1.;
            
            print "current KL_Delta is {} and prob of expert is {}".format(KL_Delta, p_expert);
            test_results = self.test_policy(featureTSs_valid,parses_valid,expert=False);
            print "[im] at iteration {}, mean {} and std {}".format(iteration,
                                test_results[0],test_results[1]);
            #embed()
            test_per_per_period.append(test_results);
            if test_results[0] <= self.min_cost: #update best model. 
                self.best_model_update();
                self.min_cost = test_results[0];
                print "[im] Update best policy, the current performance is {}".format(self.min_cost);

            #batch training:
            b_ind = 0;
            while b_ind < N_train:
                b_end_ind = np.min([b_ind+batch_size, N_train]);
                featureTSs_b = featureTSs_train[b_ind:b_end_ind];
                pad_featureTSs_b = pad_featureTSs_train[b_ind:b_end_ind];
                parses_b = parses_train[b_ind:b_end_ind];
                roll_in_inf = self.rolling_in(featureTSs = featureTSs_b, 
                        parses = parses_b,
                        expert = False, collect_optimal_ctg = True, prob_exp = p_expert);
                
                if self.imitation_train_method == 'explicit GD L2':
                    info = self.policy.fit_im_cost_sensitive(pad_featureTSs_b,
                            roll_in_inf['list_obs'],
                            roll_in_inf['list_exp_ctg'], lr = lr);
                elif self.imitation_train_method == 'explicit GD KL':
                    self.policy.fn_update_curr_params();
                    self.policy.fn_update_nabla(pad_featureTSs_b, roll_in_inf['list_obs'], roll_in_inf['list_U']);
                    info = self.policy.fit_im_natural_gradient(pad_featureTSs_b,roll_in_inf['list_obs'],
                                    roll_in_inf['list_exp_ctg'], KL_Delta = KL_Delta);

                print "[im] batch train loss is {}".format(info);
                b_ind = b_ind + batch_size;
        
        self.assign_params_to_policy(self.best_params);
        self.min_imitation_cost = self.min_cost;
        print "[im] Imitation Learning done, the min cost is {}".format(
                        self.min_imitation_cost);
        return test_per_per_period;
 

    def Policy_gradient(self, featureTSs_train, parses_train, featureTSs_valid, parses_valid,
                total_iter = 500, batch_size = 20, n_epoch = 100, 
                lr = 1e-1, KL_Delta = 0.1, pre_train = True):
        len_max = np.max([a.shape[0] for a in featureTSs_train]);
        pad_featureTSs_train = [None]*len(featureTSs_train);
        for traj_i in xrange(len(featureTSs_train)):
            pad_featureTSs_train[traj_i] = np.ones((len_max,featureTSs_train[traj_i].shape[1]))*(-np.inf);
            pad_featureTSs_train[traj_i][0:featureTSs_train[traj_i].shape[0]] = featureTSs_train[traj_i];

        print KL_Delta;
        N_train = len(featureTSs_train);
        test_per_per_period = [];
        if pre_train:
            print "##########Initialization###############";
            initial_rr = self.test_policy(featureTSs_valid,parses_valid,expert=False);
            test_per_per_period.append(initial_rr);
            print "[im] Before initialization: mean {} and std {}".format(
                                initial_rr[0],initial_rr[1]);
            if self.alg_params['system'] == 'hybird':
                self.initialize_policy(featureTSs_train,pad_featureTSs_train, parses_train, n_epch = n_epoch,
                    batch_size = 50, lr = 1e-2); #1e-4 works well for hybird. 1e-3 for eager
            else:
                self.initialize_policy(featureTSs_train,pad_featureTSs_train, parses_train, n_epch = n_epoch,
                    batch_size = 50, lr = 1e-2); #1e-4 works well for hybird. 1e-3 for eager

        print "Policy gradient start..."
        for iteration in xrange(total_iter):

            print "current KL_Delta, and lr are {},{}".format(KL_Delta,lr);
            test_results = self.test_policy(featureTSs_valid, parses_valid, expert = False);
            test_per_per_period.append(test_results);
            print "[Po] at iteration {}, mean {} and std {}".format(iteration,test_results[0],
                        test_results[1]);
            if test_results[0] < self.min_cost:
                self.best_model_update();
                self.min_cost = test_results[0];
                print "[Po] update the best policy, the current performance is {}".format(test_results[0]);

            b_ind = 0;
            while b_ind < N_train:
                b_end_ind = np.min([b_ind+batch_size,N_train]);
                featureTSs_b = featureTSs_train[b_ind:b_end_ind];
                pad_featureTSs_b = pad_featureTSs_train[b_ind:b_end_ind];
                parses_b = parses_train[b_ind:b_end_ind];
                
                if self.im_reinforce is True:
                    roll_in_inf = self.rolling_in(featureTSs = featureTSs_b, 
                        parses = parses_b,
                        expert = False, collect_optimal_ctg = True, prob_exp = 0.);
                else:
                    roll_in_inf = self.rolling_in(featureTSs = featureTSs_b, parses = parses_b,
                        expert = False, collect_optimal_ctg = False);
         
                if self.policy_train_method == 'explicit GD L2':
                    info = self.policy.fit_policy_gradient(pad_featureTSs_b, roll_in_inf['list_obs'],
                        roll_in_inf['list_U'], 
                        roll_in_inf['list_curr_ctg'], lr = lr);
                elif self.policy_train_method == 'explicit GD KL':
                    self.policy.fn_update_curr_params();
                    self.policy.fn_update_nabla(pad_featureTSs_b, roll_in_inf['list_obs'], roll_in_inf['list_U']);
                    info = self.policy.fit_policy_natural_gradient(pad_featureTSs_b, roll_in_inf['list_obs'],
                            roll_in_inf['list_U'], roll_in_inf['list_curr_ctg'], KL_Delta = KL_Delta);

                print "[im] batch train loss is {}".format(info);
                b_ind = b_ind + batch_size;
            
            
        
        self.assign_params_to_policy(self.best_params);
        print "policy gradient is done, the minimum cost is {}".format(self.min_cost);
        return test_per_per_period;
        
'''
if __name__ == '__main__':
    np.random.seed(1000);
    system = str(sys.argv[1]);
    imitation_train_method = str(sys.argv[2]);
    policy_train_method = str(sys.argv[3]);
    seed = int(sys.argv[4]);
    
    feature_name = '../HW_Parsing/hog_features_pca_50_400.save';
    [parses, featureTSs] = cPickle.load(open('../HW_Parsing/position_based_data_{}.save'.format(system)));	
    featureTSs = cPickle.load(open(feature_name));
    N = len(parses);
    parses = np.array(parses);
    featureTSs = np.array(featureTSs);

    pert = np.random.choice(len(featureTSs), len(featureTSs),replace = False);
    featureTSs_train = featureTSs[pert][:int(N*0.8)];
    parses_train = parses[pert][:int(N*0.8)];
    featureTSs_valid = featureTSs[pert][int(N*0.8):int(N*0.9)];
    parses_valid = parses[pert][int(N*0.8):int(N*0.9)];
    featureTSs_test = featureTSs[pert][int(0.9*N):];
    parses_test = parses[pert][int(N*0.9):];
    
    T = 100;
    num_stack = 1;
    num_arcs = 1;
    num_buffer = 1;
    image = False
    num_roll_in = 30;
    selector = simpleFeatureSelector(num_stack = num_stack,num_arcs=num_arcs, 
                                num_buffer = num_buffer, image = image);
    
    if system == 'hybird':
        env = arcHybirdKnownParser(num_stack=num_stack,num_arcs=num_arcs,
                                num_buffer=num_buffer,image=image);
        Expert = Expert();
    elif system == 'eager':
        env = arcEagerKnownParser(num_stack = num_stack,num_arcs=num_arcs,
                                num_buffer=num_buffer,image=image);
        Expert = Expert_Eager();
    
    num_layers = 1
    nh = 50;
    ridge = -1.;
    pre_train = True;
    real_testing = True;
    n_epoch = 10;
    lr = 1e-2*1.;
    RL_lr = 1e-2;
    KL_Delta = 1e-3*1.;
    RL_KL_Delta = 1e-4*1.;
    total_iter = 80;
    alpha = 1.0 #0.1;
    natural_reg = 0#1e-3;#1e-3;
    cg_iter = 10;
    vr = True;
    im_or_rl = 'im'
    tanh = False;
    
    params = {'system':system,'feature_file_name': feature_name, 'T':T, 
            'num_stack':num_stack, 'num_arcs':num_arcs, 'num_buffer':num_buffer,
            'image':image, 'num_roll_in':num_roll_in, 'num_layers':num_layers, 'nh':nh,
            'ridge':ridge, 'imitation_train_method':imitation_train_method,
            'policy_train_method':policy_train_method, 'pre_train':pre_train, 'n_epoch':n_epoch,
            'lr':lr, 'RL_lr':RL_lr, 'KL_Delta':KL_Delta, 'RL_KL_Delta':RL_KL_Delta, 
            'total_iter':total_iter, 'alpha':alpha,
            'cg_iter': cg_iter, 'natural_reg':natural_reg,
            'vr':vr, 'im_or_rl':im_or_rl, 'seed':seed, 'tanh':tanh};

    print params;
    tmp_obs = env.reset(featureTSs[0]);
    x_dim_1 = featureTSs[0].shape[1];
    x_dim_2 = tmp_obs.shape[0];
    num_actions = len(env.transitions);

    final_results = [];
    if real_testing:
        featureTSs_try = featureTSs_test;
        parses_try = parses_test;
    elif real_testing is False:
        featureTSs_try = featureTSs_valid;
        parses_try = parses_valid;

    pi = LSTM_Policy(x_dim_1 = x_dim_1, x_dim_2 = x_dim_2, a_dim = num_actions, output_dim = num_actions,
                        nh = params['nh'], num_roll_in = params['num_roll_in'], ridge = params['ridge'],
                        cg_iter = params['cg_iter'], reg = params['natural_reg']);

    Learner = AggreVate_parsing(env=env, policy_model = pi, expert = Expert,
                feature_selector = selector, obs_dim = x_dim_2, x_dim= x_dim_1,
                a_dim = 1,num_actions =num_actions, T = T, 
                imitation_train_method = params['imitation_train_method'],
                policy_train_method = params['policy_train_method'], params = params, 
                vr = params['vr'], testing = real_testing, im_reinforce = False);
    
    expert_perf = Learner.test_policy(featureTSs, parses, expert = True);
    print "expert performance: mean {} and std {}".format(expert_perf[0],expert_perf[1]);

    if im_or_rl == 'im':
        results_im = Learner.AggreVate_imitation(featureTSs_train = featureTSs_train, 
                    parses_train = parses_train, featureTSs_valid = featureTSs_try, 
                    parses_valid = parses_try, total_iter = params['total_iter'], 
                    batch_size=num_roll_in, pre_train=params['pre_train'],
                    n_epoch = params['n_epoch'], lr = params['lr'], KL_Delta = params['KL_Delta'],
                    alpha = params['alpha']);
        exp_im = {'expert':(expert_perf[0],expert_perf[1]), 'im':results_im}; 
    else:
        results_po = Learner.Policy_gradient(featureTSs_train=featureTSs_train, 
                    parses_train = parses_train, featureTSs_valid=featureTSs_try, 
                    parses_valid=parses_try, total_iter = params['total_iter'], 
                    batch_size = num_roll_in, n_epoch = params['n_epoch'],
                    lr = params['RL_lr'], KL_Delta = params['RL_KL_Delta'], pre_train = params['pre_train']);
        exp_im = {'expert':(expert_perf[0],expert_perf[1]), 'po':results_po}; 
    
    final_results.append(exp_im);
        #expert_perf = Learner.test_policy(featureTSs, parses, expert = True);
        #print "expert performance: mean {} and std {}".format(expert_perf[0],expert_perf[1]);

        #initial_perf = Learner.test_policy(featureTSs_test, parses_test, expert = False);
        #print "initial preformance: mean {} and std {}".format(initial_perf[0], initial_perf[1]);
        #Learner.initialize_policy(featureTSs_train, parses_train, n_epch = 100, batch_size = 50, lr = 1e-3);
        #perf = Learner.test_policy(featureTSs_test, parses_test, expert = False);
        #print "after initilization, mean {} and std {}".format(perf[0], perf[1]);
    
        #RL lr originally is 1e-5
    

 '''   
    

