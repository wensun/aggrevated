import numpy as np
from IPython import embed
import gym
import keras
import theano
import theano.tensor as T 
from keras.models import load_model
import cPickle
from modular_rl import *
from copy import deepcopy
import math
import itertools
from sklearn.linear_model import LinearRegression
 
class AggreVate_set(object):
    def __init__(self, env, policy_model, obs_dim, x_dim, a_dim, T = 100, num_roll_in = 10,
                    imitation_train_method = 'cost sensitive', 
                    policy_train_method = 'policy original',
                    setup_filter = True, scale = 1e-1, vr = False, baseline = True):
        self.policy = policy_model;
        self.env = env;
        self.env.reset()
        #self.env.seed(1000);
        self.obs_dim = obs_dim;
        self.dim_x = x_dim;
        self.dim_a = a_dim;
        self.T = T;
        self.num_of_roll_in = num_roll_in;
        self.imitation_train_method = imitation_train_method;
        self.policy_train_method = policy_train_method;
        self.num_actions = -1;
        self.Q_star = None;
        self.Expert = None;
        self.expert_obs_filter = None;
        self.expert_rew_filter = None;
        self.filter = setup_filter;
        self.obs_filter = None;
        self.scale = scale;
        self.vr = vr;

        self.best_params = []; #deepcopy(self.policy);
        for i in xrange(len(self.policy.params)):
            self.best_params.append(np.copy(self.policy.params[i].get_value()));
        self.min_cost = np.inf;
        self.min_imitation_cost = np.inf;

        self.seed_iter = itertools.count();

        self.BaseLine_LR = LinearRegression(fit_intercept=True) if baseline else None;

    def best_model_update(self):
        for i in xrange(len(self.policy.params)):
            self.best_params[i] = np.copy(self.policy.params[i].get_value());

    def assign_params_to_policy(self, params):
        assert len(params) == len(self.policy.params);
        for i in xrange(len(params)):
            self.policy.params[i].set_value(params[i]);
 
    #filters:
    def setup_filter(self):
        self.ctg_filter = ZFilter((),demean=True,destd=True,clip=1e5);
        #self.obs_filter = ZFilter(self.env.observation_space.shape,clip=5);
        #self.rew_filter = ZFilter((),demean=False,destd=True,clip=10);
        if self.expert_obs_filter is not None and self.expert_rew_filter is not None:
            self.obs_filter = deepcopy(self.expert_obs_filter); #use expert's filter is they exist'
            self.rew_filter = deepcopy(self.expert_rew_filter); #same
        else: #otherwise create new filters. 
            self.obs_filter = ZFilter(self.env.observation_space.shape,clip=5);
            self.rew_filter = ZFilter((),demean=False,destd=True,clip=10);

    def obs_filt(self, obs):
        if self.filter is True:
            return self.obs_filter(obs);
        else:
            return obs;
    
    def obs_expert_filt(self,obs):
        if self.expert_obs_filter is not None:
            return self.expert_obs_filter(obs, update = False);
        else:
            return obs;
    
    def rew_filt(self, rew):
        if self.filter is True:
            return self.rew_filter(rew);
        else:
            return rew;

    def rew_expert_filt(self, rew):
        if self.expert_rew_filter is not None:
            return self.expert_rew_filter(rew, update = False);
        else:
            return rew;
    
    def ctg_filt(self, ctg):
        if self.filter is True:
            return self.ctg_filter(ctg);
        else:
            return ctg;
    
    def load_saved_Q_star(self, save_Q_star_filename):
        #folder = '/home/wensun/Dropbox/DAggreVate/saved_Q_star/'
        folder = './saved_Q_star/'
        #folder = '~/Dropbox/DAggreVate/saved_Q_star/'
        self.Q_star = load_model(folder + save_Q_star_filename)
        if self.filter:
            self.setup_filter();

    def load_save_Expert(self, save_Expert_filename):
        #folder = '/home/wensun/Dropbox/DAggreVate/saved_Q_star/'
        folder = './saved_Q_star/'
        self.Expert = cPickle.load(open(folder+save_Expert_filename, 'rb'));
        self.expert_obs_filter = self.Expert.obfilter;
        self.expert_rew_filter = self.Expert.rewfilter;
        if self.filter:
            self.setup_filter();
        
    def pi_star(self, x): #input x is filtered
        if self.Q_star is not None:
            Q_value = self.Q_star.predict(x.reshape(1,-1))[0];
            return [np.argmax(Q_value), 1.];
        elif self.Expert is not None:
            action = self.Expert.act(x)[0];
            return [action, 1.];
    
    def pi(self, x):
        [action, prob] = self.policy.sample_action(x);
        return [action, prob]; 

    def get_state(self):
        raise NotImplementedError
    def set_state(self, state): #state is a 1-d vector
        raise NotImplementedError
        #self.env.set_state(state[0:self.env.data.qpos.shape[0]], 
            #                state[self.env.data.qpos.shape[0]:]);
    def get_obs(self):
        raise NotImplementedError    
        #obs = self.env._get_obs(); #get corresponding observation.

    def initialize_policy(self, n_epoch = 100, lr = 0.001):
        raise NotImplementedError
    
    def train(self, inputs_dic):
        raise NotImplementedError;

    def test_policy(self, num_test_roll_in = 50, expert = False):
        self.env.reset();
        return_info = self.rolling_in(expert = expert, num_roll_in = num_test_roll_in, 
                collect_optimal_ctg = False, p_expert = 0.0);
        return [np.mean(return_info['list_true_c']), np.std(return_info['list_true_c'])];    

    def optimal_ctg_MC(self, state, a, steps_left = 100):  #return Q(s, a).
        #assert self.Expert is not None;
        trajs_ctg = [];
        for repeat in xrange(0, np.min([int(self.num_of_roll_in), 1])):
            self.set_state(state);
            obs = self.get_obs();
            act = a;
            ctg = 0.0;
            for t in xrange(0, steps_left):
                obs,reward,done,info = self.env.step(act);
                reward = -np.sign(reward) * 0. if done else reward;  #if reward is postive, then when done (failed), reward should be negative. if reward is negative, when done (success), the reward should be postivie. 
                ctg = ctg + (-self.rew_filt(reward)); #translate to cost, instead of reward. 
                obs = self.obs_expert_filt(obs); #filtering:
                act = self.pi_star(obs)[0]; 
                #self.Expert.predict(obs.reshape(1,-1))[0]; #predict based on current obs.
                if done is True:
                    break;
            ctg = self.ctg_filt(ctg); #filter every ctg.
            trajs_ctg.append(ctg);
        self.set_state(state);#re-set the env back to the feeded state. 
        return np.mean(trajs_ctg), np.std(trajs_ctg); #return mean and std of ctgs.

    def rolling_in(self, expert = False, num_roll_in = 10, 
                        collect_optimal_ctg = True, 
                        MC = False, p_expert = 0):
        trajs_obs = [];#list of matrix, for each traj, store obs_t, for all t.
        trajs_state = []; #list of matrix, for each traj, store state_t, for all t
        trajs_a = []; #list of matrix or vector, for each traj, store a_t, for all t
        trajs_a_expert = [];
        trajs_rew = []; #list of vector, for each traj, store r_t for all t
        trajs_true_c = []; #unnormalized reward: only for testing.
        trajs_cc = []; #list of scalar, for each traj, store \sum_t c_t: total cost 
        trajs_prob = []; #list of vector, for each trajectory, store \pi(a_t | s_t; curr_\theta), for all t
        trajs_exp_ctg = []; #list of vector, for each trajtory, store Q^*(s_t, a_t), for all t. 
        
        for i in xrange(num_roll_in): 
            #np.random.seed(self.seed_iter.next());
            obs = self.env.reset();    
            state = self.get_state();
            traj_obs = np.zeros((self.T, obs.shape[0]));
            
            if self.dim_a == 1:
                traj_a = np.ones(self.T)*(-np.inf);
                traj_a_expert = np.ones(self.T)*(-np.inf);
            else:
                traj_a = np.ones((self.T, self.dim_a))*(-np.inf);
                traj_a_expert = np.ones((self.T, self.dim_a))*(-np.inf);

            traj_state = np.zeros((self.T, state.shape[0]));
            traj_prob = np.zeros(self.T);
            traj_rew = np.zeros(self.T);
            traj_cc = 0.0;
            traj_true_c = 0.0;
            traj_len = 0;
            for t in xrange(0, self.T):
                p_exp = np.random.rand(); 
                if expert is True or p_exp <= p_expert:
                    obs = self.obs_expert_filt(obs); # if expert is True else self.obs_filt(obs); #filter.
                    [act, prob_a] = self.pi_star(obs);# if expert is True else self.pi(obs);
                    act_expert = self.pi_star(obs)[0];
                else:
                    obs = self.obs_filt(obs);
                    [act, prob_a] = self.pi(obs);
                    act_expert = self.pi_star(obs)[0];

                traj_obs[t] = obs;
                traj_a[t] = act;
                traj_a_expert[t] = act_expert;
                traj_state[t] = self.get_state();
                traj_prob[t] = prob_a;
                traj_len = traj_len + 1;

                obs,reward,done,info = self.env.step(act);
                reward = -np.sign(reward) * 0 if done else reward;  #if reward is postive, then when done (failed), reward should be negative. if reward is negative, when done (success), the reward should be postivie. 
                traj_rew[t] = reward;
                traj_true_c = traj_true_c + (-reward);
                traj_cc = traj_cc + (-self.rew_filt(reward)); #decay gamma. np.power(self.gamma,t) *   
                if done:
                    break;
            
            if self.Q_star is not None and self.dim_a == 1 and collect_optimal_ctg is True:
                cost_to_go_vec = np.zeros((len(traj_obs),self.num_actions));
                if MC is False:
                    cost_to_go_vec[0:traj_len] = -1. * self.Q_star.predict(traj_obs[0:traj_len]);
                    if self.vr is True:
                        cost_to_go_vec = cost_to_go_vec - np.tile(np.min(cost_to_go_vec,axis=1),(self.num_actions,1)).T;
                else: #use MC samples to estimate Q^*
                    print "collecting cost-to-go using MC "
                    for step in xrange(traj_len):
                        for a_i in xrange(self.num_actions):
                            cost_to_go_vec[step, a_i] = self.optimal_ctg_MC(
                                    traj_state[step], a_i, 
                                    steps_left = self.T - step+1)[0];
                trajs_exp_ctg.append(cost_to_go_vec);
            
            elif self.Expert is not None and collect_optimal_ctg is True:
                traj_ctg = np.zeros(self.T);
                #print "collecting cost-to-go using MC "
                for step in xrange(traj_len):
                    [ctg_m, ctg_std] = self.optimal_ctg_MC(traj_state[step],traj_a[step], 
                    steps_left = self.T-step+1);
                    if self.vr is True:#compute advantage:
                        ctg_m = ctg_m -self.optimal_ctg_MC(traj_state[step],traj_a_expert[step])[0]; #Q - V.
                    traj_ctg[step] = ctg_m;
                trajs_exp_ctg.append(traj_ctg);
            
            trajs_obs.append(traj_obs);
            trajs_state.append(traj_state);
            trajs_a.append(traj_a);
            trajs_a_expert.append(traj_a_expert);
            trajs_rew.append(traj_rew);
            trajs_cc.append(traj_cc);
            trajs_true_c.append(traj_true_c);
            trajs_prob.append(traj_prob);
        
        self.env.reset();
        return {'list_obs':trajs_obs, 'list_state':trajs_state, 'list_U':trajs_a, 
                'list_tc':trajs_cc, 'list_exp_ctg':trajs_exp_ctg, 'list_u_probs':trajs_prob, 
                'list_true_c':trajs_true_c, 'list_rew':trajs_rew};
        
    def filter_stack_valid_states(self, inputs_dic, exp_ctg = True):
        for i in xrange(len(inputs_dic['list_obs'])):
            if inputs_dic['list_U'][i].ndim > 1:
                valid_len = inputs_dic['list_U'][i].shape[0] - np.sum(inputs_dic['list_U'][i][:,0] == -np.inf);
            else:
                valid_len = inputs_dic['list_U'][i].shape[0] - np.sum(inputs_dic['list_U'][i] == -np.inf);
            stacked_obs = inputs_dic['list_obs'][i][:valid_len] if i == 0 else np.concatenate([stacked_obs,inputs_dic['list_obs'][i][:valid_len]],axis=0);
            if exp_ctg:
                stacked_exp_ctg = inputs_dic['list_exp_ctg'][i][:valid_len] if i == 0 else np.concatenate([stacked_exp_ctg, inputs_dic['list_exp_ctg'][i][:valid_len]],axis=0); 
            else:
                stacked_exp_ctg = None;
            stacked_a = inputs_dic['list_U'][i][:valid_len] if i == 0 else np.concatenate([stacked_a,inputs_dic['list_U'][i][:valid_len]],axis =0);
            stacked_prob=inputs_dic['list_u_probs'][i][:valid_len] if i == 0 else np.concatenate([stacked_prob,inputs_dic['list_u_probs'][i][:valid_len]],axis = 0);

            valid_rew = inputs_dic['list_rew'][i][:valid_len];
            emp_ctg = np.array([-np.sum(valid_rew[j:]) for j in range(0, valid_rew.shape[0])]);
            stacked_emp_ctg=emp_ctg if i == 0 else np.concatenate([stacked_emp_ctg,emp_ctg]);

        if self.BaseLine_LR is not None:
            self.BaseLine_LR.fit(stacked_obs, stacked_emp_ctg);
            stacked_emp_ctg = stacked_emp_ctg - self.BaseLine_LR.predict(stacked_obs);
            if exp_ctg is True:
                self.BaseLine_LR.fit(stacked_obs, stacked_exp_ctg);
                stacked_exp_ctg = stacked_exp_ctg - self.BaseLine_LR.predict(stacked_obs);

        return [stacked_obs, stacked_exp_ctg, stacked_a, stacked_prob, stacked_emp_ctg];
    

    def im_train(self, stacked_obs,stacked_ctg,stacked_a,
                stacked_prob, mu, num_epoch = 50, 
                lr = 0.1, KL_Delta = 0.1):
        #embed();
        #print self.imitation_train_method
        if self.imitation_train_method == 'explicit GD L2':
            #print lr
            output = self.policy.fit_im_cost_sensitive(
                            X = stacked_obs, Y = stacked_ctg,
                            U = stacked_a, Probs = stacked_prob,
                            n_epoch = 1, lr = lr);
            
        elif self.imitation_train_method == 'implicit GD KL':
            print lr;
            #implicitly solve the proximal operator using SGD with n_epoch. 
            output = self.policy.fit_im_cost_sensitive_proximal(
                        X = stacked_obs, Y = stacked_ctg,
                        U = stacked_a, Probs = stacked_prob,
                        n_epoch = num_epoch, lr = lr, mu = mu, 
                        KL_Delta = KL_Delta);
        
        elif self.imitation_train_method == 'explicit GD KL':
            output = self.policy.fit_im_natural_gradient(
                        X = stacked_obs, Y = stacked_ctg,
                        U = stacked_a, Probs = stacked_prob, n_epoch = 1, 
                        KL_Delta = KL_Delta);
            #if output['flag'] is True:
            #    print "Exceeds the KL_Delta, either decrease the lr or reduce the number of iterations";
        return output;

    def AggreVate_imitation(self, total_iter = 500, pre_train = True, n_epoch = 100, 
                            lr = 1e-1, KL_Delta = 0.1, MC = False, alpha = 1.):
        
        test_per_per_period = [];
        if pre_train:
            print "########Initialization###############";
            initial_rr = self.test_policy(num_test_roll_in = 100, expert = False);
            test_per_per_period.append(initial_rr);
            print "[im] Before initialization: mean {} and std {}".format(
                                initial_rr[0],initial_rr[1]);
            self.initialize_policy(n_epoch = 2000, lr =1e-4);
            after_init = self.test_policy(num_test_roll_in = 200, expert = False);
            print "[im] After initilization: mean {} and std {}".format(
                                after_init[0], after_init[1]);

        original_KL_Delta = KL_Delta;
        original_lr = lr;
        print "Start AggreVate....";
        for e in xrange(total_iter):
            #test the curr policy's performance by rolling-out:
            curr_cost_mean_std = self.test_policy(
                            num_test_roll_in = 200,
                                    expert = False);
            print '[im] Cummulative Cost: mean {} and std {}, at iter {}'.format(
                curr_cost_mean_std[0], curr_cost_mean_std[1],e
            );
            test_per_per_period.append((curr_cost_mean_std[0], 
                                        curr_cost_mean_std[1]));
            if curr_cost_mean_std[0] < self.min_cost:
                self.best_model_update();
                self.min_cost = curr_cost_mean_std[0];
                print "[Im] Update the best policy, the current performance is {}".format(self.min_cost);
            
            #prepare for next policy update:
            p_expert = (1.0 - alpha)**(e+1); 
            
            #decaying learning rate:
            if np.mod(e+1,10) == 0:
                lr = lr / 1.;

            if e <= total_iter / 2.:
                KL_Delta = original_KL_Delta;
            else:
                KL_Delta = original_KL_Delta / 1.; 

            print "currnt p_expert is {}, and KL_Delta is {} and lr is {}".format(p_expert,KL_Delta,lr);

            #[trajs_obs,trajs_state,trajs_a,trajs_fcc,trajs_ctg,trajs_prob, trajs_true_cc] 
            roll_in_inf = self.rolling_in(expert = False, 
                                        num_roll_in = self.num_of_roll_in,
                                        collect_optimal_ctg=True, MC = MC, p_expert = p_expert);
                                            
            c = self.scale;
            mu = c/(total_iter + 1.); #more stable, less aggressive choice.
            [stacked_obs, stacked_exp_ctg, stacked_a, stacked_prob,stacked_emp_ctg] = self.filter_stack_valid_states(
                            roll_in_inf);
            
            if self.imitation_train_method != 'explicit GD L2': #implicit natural GD:
                self.policy.fn_update_curr_params();
                self.policy.fn_update_nabla(np.array(roll_in_inf['list_obs']), 
                                    np.array(roll_in_inf['list_U']));

            info = self.im_train(stacked_obs,stacked_exp_ctg,stacked_a,
                    stacked_prob, mu, 
                    num_epoch = n_epoch, lr = lr, KL_Delta = KL_Delta); #performs one gradient-bassed updates (e.g., gradient descent, natural GD, or implicit natural GD)
            print 'loss at AggreVate iteration {} is {}'.format(e, info['loss'])

            #if math.isnan(info['loss']):
            #    self.assign_params_to_policy(self.best_params);#roll back to the best one. 
            #    lr = lr * 0.1;
            #    n_epoch = 2;

            #if info['flag'] is False:
            #    n_epoch = np.min([2000, n_epoch * 2]);
            #    print "Increased n_epoch to {}, due to KL-Delta not being reached".format(n_epoch);

        self.assign_params_to_policy(self.best_params);
        self.min_imitation_cost = self.min_cost;
        print "[im] Imitation Learning done, the min cost is {}".format(
                        self.min_imitation_cost);
        return test_per_per_period;

    #######################Policy Gradient #################################
    def policy_train(self, stacked_obs, stacked_a, stacked_emp_ctg, mu,
            num_epoch = 50,  lr = 0.1, KL_Delta = 0.1):
        if self.policy_train_method == 'explicit GD L2':
            output = self.policy.fit_policy_gradient(X = stacked_obs,
                    U = stacked_a, ctgs = stacked_emp_ctg, lr = lr);
        elif self.policy_train_method == 'implicit GD KL':
            output = self.policy.fit_policy_proximal(X = stacked_obs,
                    U = stacked_a, ctgs = stacked_emp_ctg, 
                    n_epoch = num_epoch, lr = lr, mu = mu, KL_Delta = KL_Delta);
        elif self.policy_train_method == 'explicit GD KL':
            output = self.policy.fit_policy_natural_gradient(X=stacked_obs,
                    U = stacked_a, ctgs = stacked_emp_ctg,
                    n_epoch = 1, KL_Delta = KL_Delta);        
        return output;
            
    def Policy_gradient(self, total_iter = 500, n_epoch = 100, 
                        lr = 1e-1,  KL_Delta = 0.1, MC = False):
        test_per_per_period = [];
        for e in xrange(total_iter):
            roll_in_info = self.rolling_in(expert = False, 
                                        num_roll_in = self.num_of_roll_in,
                                        collect_optimal_ctg=False, MC = MC);
                                        #we only need to get sample trajectories, 
                                        #we do not need Q^* in policy gradient.
            
            curr_cost_mean_std = self.test_policy(
                            num_test_roll_in = 200,
                                    expert = False);
            print '[rl] Cummulative Cost: mean {} and std {}, at iter {}'.format(
                curr_cost_mean_std[0], curr_cost_mean_std[1],e
            );
            test_per_per_period.append((curr_cost_mean_std[0], 
                                        curr_cost_mean_std[1]));

            if curr_cost_mean_std[0] < self.min_cost:
                self.best_model_update();
                self.min_cost = curr_cost_mean_std[0];
                print "[rl] Update the best policy, the current performance is {}".format(self.min_cost);

            mu = 1./np.power(e + 1., 0.5); #parameter for regularization.
            [stacked_obs, stacked_exp_ctg, stacked_a, stacked_prob, stacked_emp_ctg] = self.filter_stack_valid_states(
                            roll_in_info, exp_ctg = False);

            if self.policy_train_method != 'explicit GD L2': #implicit natural GD:
                self.policy.fn_update_curr_params();
                self.policy.fn_update_nabla(
                                np.array(roll_in_info['list_obs']), 
                                np.array(roll_in_info['list_U']));

            info = self.policy_train(stacked_obs, stacked_a, stacked_emp_ctg, mu = mu, 
                                    num_epoch = n_epoch,
                                    lr = lr, KL_Delta = KL_Delta);
            print 'loss at rl iteration {} is {}'.format(e, info['loss'])
            
            if info['flag'] is False:
                n_epoch = np.min([2000, n_epoch * 2]);
                print "Increased n_epoch to {}, due to KL-Delta not being reached".format(
                    n_epoch);

        return test_per_per_period;

