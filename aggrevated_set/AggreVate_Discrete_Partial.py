import numpy as np
from IPython import embed
import gym
#from single_RNN_policy import LSTM_based_Policy
from AggreVate import AggreVate_set
import sys


#create a partial observable setting from openai gym model CartPole and Acrobot. 
class partial_environment(object):
    def __init__(self, openAI_Gym_model_name, obs_index = [0,1,2]):
        self.model_name = openAI_Gym_model_name;
        self.env = gym.make(openAI_Gym_model_name);
        self.obs_index = obs_index;

        self.env.reset();
    
    def seed(self, seed):
        self.env.seed(seed);

    @property
    def state(self):
        if self.model_name == 'CartPole-v0':
            return self.env.state;
        elif self.model_name == 'Acrobot-v1':
            return self.env._get_ob();
    
    def _get_ob(self):
        return self.state[self.obs_index];
    
    def reset(self):
        obs = self.env.reset();
        return obs;

    def step(self, a):
        ori_obs, r, done, info = self.env.step(a);
        return ori_obs[[self.obs_index]],r,done,info;
    
    @property
    def n_actions(self):
        return self.env.action_space.n;


class AggreVate_Discrete_Partial(AggreVate_set):
    def __init__(self, env, policy_model, obs_dim, x_dim, a_dim, num_actions, 
                    T = 100, num_roll_in = 10,
                    imitation_train_method = 'explicit GD L2', 
                    policy_train_method = 'explicit GD L2', 
                    setup_filter = False, vr = False):
        
        AggreVate_set.__init__(self, env, policy_model, 
                            obs_dim, x_dim, a_dim, T, 
                            num_roll_in, imitation_train_method, 
                            policy_train_method, setup_filter, vr = vr);
        
        self.num_actions = num_actions;
    
    def get_state(self):
        return np.array(self.env.state);
    
    def set_state(self, state):
        self.env.state = state;
    
    def get_obs(self):
        if self.obs_dim == self.dim_x: #state and obs are the same:
            return self.env.state;
        else:
            return self.env._get_ob(); #Acrobot Has this _get_ob interface to return obs. 

    def optimal_ctg_MC(self, state, a, steps_left = 100):  #return Q(s, a).
        #assert self.Expert is not None;
        trajs_ctg = [];
        for repeat in xrange(0, np.min([int(self.num_of_roll_in), 1])):
            self.set_state(state);
            obs = self.get_obs();
            state = self.get_state();
            act = a;
            ctg = 0.0;
            for t in xrange(0, steps_left):
                obs,reward,done,info = self.env.step(act);
                state = self.get_state();
                reward = -np.sign(reward) * 0. if done else reward;  #if reward is postive, then when done (failed), reward should be negative. if reward is negative, when done (success), the reward should be postivie. 
                ctg = ctg + (-self.rew_filt(reward)); #translate to cost, instead of reward. 
                state = self.obs_expert_filt(state); #filtering:
                act = self.pi_star(state)[0]; 
                #self.Expert.predict(obs.reshape(1,-1))[0]; #predict based on current obs.
                if done is True:
                    break;
            ctg = self.ctg_filt(ctg); #filter every ctg.
            trajs_ctg.append(ctg);
        self.set_state(state);#re-set the env back to the feeded state. 
        return np.mean(trajs_ctg), np.std(trajs_ctg); #return mean and std of ctgs.


    def rolling_in(self, expert = False, num_roll_in = 10, 
                        collect_optimal_ctg = True, 
                        MC = False, p_expert = 0):
        trajs_obs = [];#list of matrix, for each traj, store obs_t, for all t.
        trajs_state = []; #list of matrix, for each traj, store state_t, for all t
        trajs_a = []; #list of matrix or vector, for each traj, store a_t, for all t
        trajs_a_expert = [];
        trajs_rew = []; #list of vector, for each traj, store r_t for all t
        trajs_true_c = []; #unnormalized reward: only for testing.
        trajs_cc = []; #list of scalar, for each traj, store \sum_t c_t: total cost 
        trajs_prob = []; #list of vector, for each trajectory, store \pi(a_t | s_t; curr_\theta), for all t
        trajs_exp_ctg = []; #list of vector, for each trajtory, store Q^*(s_t, a_t), for all t. 
        trajs_ctg = [];

        for i in xrange(num_roll_in): 
            #np.random.seed(self.seed_iter.next());
            self.policy.reset();
            self.env.reset();    
            state = self.get_state();
            obs = self.get_obs();
            
            traj_obs = np.zeros((self.T, obs.shape[0]));
            if self.dim_a == 1:
                traj_a = np.ones(self.T)*(-np.inf);
                #traj_a_expert = np.ones(self.T)*(-np.inf);
            else:
                traj_a = np.ones((self.T, self.dim_a))*(-np.inf);
                #traj_a_expert = np.ones((self.T, self.dim_a))*(-np.inf);

            traj_state = np.zeros((self.T, state.shape[0]));
            traj_prob = np.zeros(self.T);
            traj_rew = np.zeros(self.T);
            traj_cc = 0.0;
            traj_true_c = 0.0;
            traj_len = 0;
            traj_ctg = np.zeros(self.T);
            
            for t in xrange(0, self.T):
                p_exp = np.random.rand(); 
                if expert is True or p_exp <= p_expert:
                    state = self.obs_expert_filt(state); # if expert is True else self.obs_filt(obs); #filter.
                    [act, prob_a] = self.pi_star(state);# if expert is True else self.pi(obs);
                    #act_expert = self.pi_star(state)[0];
                else:
                    obs = self.obs_filt(obs);
                    [act, prob_a] = self.pi(obs);
                    #act_expert = self.pi_star(state)[0];

                traj_obs[t] = obs;
                traj_a[t] = act;
                #traj_a_expert[t] = act_expert;
                traj_state[t] = self.get_state();
                traj_prob[t] = prob_a;
                traj_len = traj_len + 1;

                obs,reward,done,info = self.env.step(act);
                state = self.get_state();

                reward = -np.sign(reward) * 0 if done else reward;  #if reward is postive, then when done (failed), reward should be negative. if reward is negative, when done (success), the reward should be postivie. 
                traj_rew[t] = reward;
                traj_ctg[t] = np.sum(-traj_rew[:traj_len]);
                traj_true_c = traj_true_c + (-reward);
                traj_cc = traj_cc + (-self.rew_filt(reward)); #decay gamma. np.power(self.gamma,t) *   
                if done:
                    break;
            
            if self.Q_star is not None and self.dim_a == 1 and collect_optimal_ctg is True:
                cost_to_go_vec = np.zeros((len(traj_obs),self.num_actions));
                if MC is False:
                    cost_to_go_vec[0:traj_len] = -1. * self.Q_star.predict(traj_state[0:traj_len]);
                    if self.vr is True:
                        cost_to_go_vec = cost_to_go_vec - np.tile(np.min(cost_to_go_vec,axis=1),(self.num_actions,1)).T;
                else: #use MC samples to estimate Q^*
                    print "collecting cost-to-go using MC "
                    for step in xrange(traj_len):
                        for a_i in xrange(self.num_actions):
                            cost_to_go_vec[step, a_i] = self.optimal_ctg_MC(
                                    traj_state[step], a_i, 
                                    steps_left = self.T - step+1)[0];
                trajs_exp_ctg.append(cost_to_go_vec);
            
            elif self.Expert is not None and collect_optimal_ctg is True:
                traj_exp_ctg = np.zeros(self.T);
                #print "collecting cost-to-go using MC "
                for step in xrange(traj_len):
                    [ctg_m, ctg_std] = self.optimal_ctg_MC(traj_state[step],traj_a[step], 
                    steps_left = self.T-step+1);
                    if self.vr is True:#compute advantage:
                        ctg_m = ctg_m -self.optimal_ctg_MC(traj_state[step],traj_a_expert[step])[0]; #Q - V.
                    traj_exp_ctg[step] = ctg_m;
                trajs_exp_ctg.append(traj_exp_ctg);
            
            trajs_obs.append(traj_obs);
            trajs_state.append(traj_state);
            trajs_a.append(traj_a);
            #trajs_a_expert.append(traj_a_expert);
            trajs_rew.append(traj_rew);
            trajs_cc.append(traj_cc);
            trajs_true_c.append(traj_true_c);
            trajs_prob.append(traj_prob);
            trajs_ctg.append(traj_ctg);
        
        self.env.reset();
        return {'list_obs':trajs_obs, 'list_state':trajs_state, 'list_U':trajs_a, 
                'list_tc':trajs_cc, 'list_exp_ctg':trajs_exp_ctg, 'list_u_probs':trajs_prob, 
                'list_true_c':trajs_true_c, 'list_rew':trajs_rew, 'list_ctg':trajs_ctg};


    def AggreVate_imitation(self, total_iter = 500, pre_train = True, n_epoch = 100, 
                            lr = 1e-1, KL_Delta = 0.1, MC = False, alpha = 1.):
        
        test_per_per_period = [];
        print "Start AggreVate....";
        for e in xrange(total_iter):
            #test the curr policy's performance by rolling-out:
            curr_cost_mean_std = self.test_policy(
                            num_test_roll_in = 200,
                                    expert = False);
            print '[im] Cummulative Cost: mean {} and std {}, at iter {}'.format(
                curr_cost_mean_std[0], curr_cost_mean_std[1],e
            );
            test_per_per_period.append((curr_cost_mean_std[0], 
                                        curr_cost_mean_std[1]));
            if curr_cost_mean_std[0] < self.min_cost:
                self.best_model_update();
                self.min_cost = curr_cost_mean_std[0];
                print "[Im] Update the best policy, the current performance is {}".format(self.min_cost);
            
            #prepare for next policy update:
            p_expert = (1.0 - alpha)**(e+1); 
            print "currnt p_expert is {}, and KL_Delta is {} and lr is {}".format(p_expert,KL_Delta,lr);

            #[trajs_obs,trajs_state,trajs_a,trajs_fcc,trajs_ctg,trajs_prob, trajs_true_cc] 
            roll_in_inf = self.rolling_in(expert = False, 
                                        num_roll_in = self.num_of_roll_in,
                                        collect_optimal_ctg=True, MC = MC, p_expert = p_expert);

            if self.imitation_train_method == 'explicit GD L2':
                output = self.policy.fit_im_cost_sensitive(roll_in_inf['list_obs'],
                        roll_in_inf['list_exp_ctg'], lr = lr);
            
            elif self.imitation_train_method == 'explicit GD KL':
                self.policy.fn_update_curr_params();
                self.policy.fn_update_nabla(roll_in_inf['list_obs'],roll_in_inf['list_U']);
                output = self.policy.fit_im_natural_gradient(roll_in_inf['list_obs'],
                        roll_in_inf['list_exp_ctg'], KL_Delta = KL_Delta);
            else:
                print "doesn't support the algorithm";
                assert False;
            
            print '[im] current batch loss is {}'.format(output);
        
        self.assign_params_to_policy(self.best_params);
        self.min_imitation_cost = self.min_cost;
        print "[im] Imitation Learning done, the min cost is {}".format(
                        self.min_imitation_cost);
        return test_per_per_period;

    
    def Policy_gradient(self, total_iter = 500, n_epoch = 100, 
                        lr = 1e-1,  KL_Delta = 0.1, MC = False):
        test_per_per_period = [];
        for e in xrange(total_iter):
            roll_in_info = self.rolling_in(expert = False, 
                                        num_roll_in = self.num_of_roll_in,
                                        collect_optimal_ctg=False, MC = MC);
                                        #we only need to get sample trajectories, 
                                        #we do not need Q^* in policy gradient.
            
            curr_cost_mean_std = self.test_policy(
                            num_test_roll_in = 200,
                                    expert = False);
            print '[rl] Cummulative Cost: mean {} and std {}, at iter {}'.format(
                curr_cost_mean_std[0], curr_cost_mean_std[1],e
            );
            test_per_per_period.append((curr_cost_mean_std[0], 
                                        curr_cost_mean_std[1]));

            if curr_cost_mean_std[0] < self.min_cost:
                self.best_model_update();
                self.min_cost = curr_cost_mean_std[0];
                print "[rl] Update the best policy, the current performance is {}".format(self.min_cost);

            if self.policy_train_method == 'explicit GD L2':
                output = self.policy.fit_policy_gradient(roll_in_info['list_obs'],
                        roll_in_info['list_U'], roll_in_info['list_ctg'], lr=lr);

            if self.policy_train_method == 'explicit GD KL': #implicit natural GD:
                self.policy.fn_update_curr_params();
                self.policy.fn_update_nabla(roll_in_info['list_obs'],roll_in_info['list_U']);
                output = self.policy.fit_policy_natural_gradient(
                    roll_in_info['list_obs'],roll_in_info['list_U'], roll_in_info['list_ctg'],
                    KL_Delta = KL_Delta);

            print 'loss at rl iteration {} is {}'.format(e, output);
        return test_per_per_period;


'''
if __name__ == '__main__':

    np.random.seed(1000);
    openAI_model_name = str(sys.argv[1]);
    #= 'Acrobot-v1';
    #openAI_model_name = 'CartPole-v0';
    obs_index = [0,2] if openAI_model_name == 'CartPole-v0' else [0,1,2,3];
    env = partial_environment(openAI_model_name, obs_index = obs_index);
    env.seed(1000)
    obs_dim = len(obs_index);
    x_dim = env.state.shape[0];
    
    a_dim = 1;
    num_actions = env.n_actions;
    if openAI_model_name == 'Acrobot-v1':
        T = 200;
        Q_star_name = 'acrobot_keras_model_Q_star_vector.h';
    elif openAI_model_name == 'CartPole-v0':
        T = 500;
        Q_star_name = 'cartpole_keras_model_Q_star_vector.h'

    num_roll_in = 100;
    imitation_train_method = str(sys.argv[2]);
    policy_train_method = str(sys.argv[3]);
    po_or_im = str(sys.argv[4]);

    params = {'num_roll_in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[16], 
    'evn':openAI_model_name, 'expert':Q_star_name, 
    'n_epoch':200, 'im_lr':1e-2*5.,'po_lr':1e-3,
    'KL Delta':1e-2*5., 'Agg_iter':50, 
    'Pre-Train':False,
    'filter':False, 'vr':True, 'reg':0.0, 'cg_iter':20};    
    print params;

    final_results = [];

    for repeat in xrange(5):
        env = partial_environment(openAI_model_name, obs_index = obs_index);
        env.seed(repeat);
        np.random.seed(repeat);
        print "at repeat {}".format(repeat);
        
        Pi = LSTM_based_Policy(x_dim = obs_dim, a_dim = a_dim, output_dim = num_actions,
            nh = 16, num_roll_in = params['num_roll_in'], cg_iter= params['cg_iter'],
            reg = params['reg'], discrete = True);

        Learner = AggreVate_Discrete_Partial(env = env, policy_model = Pi, 
                obs_dim = len(obs_index), x_dim = x_dim, a_dim = a_dim, num_actions = num_actions,
                T = params['T'], num_roll_in = params['num_roll_in'], 
                imitation_train_method = params['imitation'], 
                policy_train_method = params['policy'], 
                setup_filter = params['filter'], vr = params['vr']);
        
        Learner.load_saved_Q_star(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 50, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);
    
        #embed()

        if po_or_im == 'im':
            print "perform imitation"
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                            pre_train = params['Pre-Train'], 
                            n_epoch = params['n_epoch'], lr = params['im_lr'], 
                            KL_Delta = params['KL Delta'], 
                            MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'im':im_rr}#, 'po':po_rr};
        
        elif po_or_im == 'rl':   
            print "perform RL"
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'po':po_rr};

        final_results.append(exp_im_po);

    #filername = 'result_Model_{}_numrollin_{}_T_{}_method_{}_expert_{}_n_epoch_{}_imlr_{}_polr_{}_KLD_{}_PT_{}_F_{}'.format(
    #        params['evn'],params['num roll in'],params['T'],params['imitation'], params['expert'],
    #        params['n_epoch'],params['im_lr'], params['po_lr'],params['KL Delta'], 
    #        params['Pre-Train'], params['filter']);
'''

