import numpy as np
from IPython import embed
import gym
#from policy_discrete import Policy_discrete
from AggreVate import AggreVate_set
import sys

class AggreVate_discrete(AggreVate_set):
    '''
    This is mainly designed for discrete action, it currently supports 
    OpenAI Gym's CartPole, MountainCar, and Acrobot. 
    '''
    def __init__(self, env, policy_model, obs_dim, x_dim, a_dim, num_actions, 
                    T = 100, num_roll_in = 10,
                    imitation_train_method = 'explicit GD L2', 
                    policy_train_method = 'explicit GD L2', 
                    setup_filter = False, vr = False):
        
        AggreVate_set.__init__(self, env, policy_model, 
                            obs_dim, x_dim, a_dim, T, 
                            num_roll_in, imitation_train_method, 
                            policy_train_method, setup_filter, vr = vr);
        
        self.num_actions = num_actions;

    def get_state(self):
        return self.env.state;
    
    def set_state(self, state):
        self.env.state = state;
    
    def get_obs(self):
        if self.obs_dim == self.dim_x: #state and obs are the same:
            return self.env.state;
        else:
            return self.env._get_ob(); #Acrobot Has this _get_ob interface to return obs. 
    
    def initialize_policy(self, n_epch = 32, lr = 0.001, MC = False):
        [trajs_obs,trajs_state,trajs_a,trajs_cc,trajs_ctg,trajs_prob] = self.rolling_in(expert = True, 
                    num_roll_in = self.num_of_roll_in, MC = MC);
        
        assert len(trajs_obs) == len(trajs_ctg);
        stacked_obs = np.concatenate([p for p in trajs_obs], axis = 0);
        stacked_ctgs=np.concatenate([p for p in trajs_ctg], axis = 0); 
        self.policy.fit_im_cost_sensitive(stacked_obs, 
                            stacked_ctgs, n_epoch =32, lr = lr);
    

'''
if __name__ == '__main__':
    np.random.seed(1000);
    openAI_model_name = 'Acrobot-v1';
    #openAI_model_name = 'MountainCar-v0';
    #openAI_model_name = 'CartPole-v0';
    env = gym.make(openAI_model_name);
    env.seed(1000)
    obs_dim = env.reset().shape[0];
    x_dim = env.state.shape[0];
    
    #Q_star_name = 'cartpole_keras_model_Q_star_vector.h'
    #Q_star_name = 'acrobot_keras_model_Q_star_vector.h';
    #Q_star_name = 'mountain_car_keras_model_Q_star_vector.h';
    a_dim = 1;
    num_actions = env.action_space.n;

    if openAI_model_name == 'Acrobot-v1':
        T = 200;
        Q_star_name = 'acrobot_keras_model_Q_star_vector.h';
    elif openAI_model_name == 'CartPole-v0':
        T = 500;
        Q_star_name = 'cartpole_keras_model_Q_star_vector.h'

    num_roll_in = 100;
    #imitation_train_method = 'implicit GD KL';
    imitation_train_method = str(sys.argv[1]);
    #imitation_train_method = 'explicit GD L2';
    #'explicit GD KL'  #'explicit GD L2'; #'implicit GD KL'; 
    #policy_train_method = 'explicit GD L2';
    policy_train_method = str(sys.argv[2]);
    po_or_im = str(sys.argv[3]);

    params = {'num roll in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[16], 
    'evn':openAI_model_name, 'expert':Q_star_name, 
    'n_epoch':200, 'im_lr':1e-1*2.,'po_lr':1e-1*2.,'KL Delta':1e-3*5., 'Agg_iter':20, 'Pre-Train':False,
    'filter':False, 'vr':True, 'reg':0.0, 'cg_iter':20};    
    print params;

    final_results = [];
    for repeat in xrange(10):
        env = gym.make(openAI_model_name);
        env.seed(repeat);
        np.random.seed(repeat);
        print "at repeat {}".format(repeat);
        pi = Policy_discrete(dx = obs_dim, k = num_actions, num_layers = params['num hid Layer'], 
                            nh = params['nh'], 
                            num_roll_in = params['num roll in'], cg_iter = params['cg_iter'], reg = params['reg']);
        Learner = AggreVate_discrete(env = env, policy_model = pi, obs_dim = obs_dim, 
                                x_dim = x_dim, a_dim = a_dim, 
                                num_actions = num_actions, T = params['T'], 
                                num_roll_in = params['num roll in'], 
                                imitation_train_method = params['imitation'],
                                policy_train_method = params['policy'], 
                                setup_filter = params['filter'], vr = params['vr']);
    
        Learner.load_saved_Q_star(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 50, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);
    
        if po_or_im == 'im':
            print "perform imitation"
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                            pre_train = params['Pre-Train'], 
                            n_epoch = params['n_epoch'], lr = params['im_lr'], 
                            KL_Delta = params['KL Delta'], 
                            MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'im':im_rr}#, 'po':po_rr};
        elif po_or_im == 'rl':
            print "perform RL"
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'po':po_rr};

        final_results.append(exp_im_po);

    filername = 'result_Model_{}_numrollin_{}_T_{}_method_{}_expert_{}_n_epoch_{}_imlr_{}_polr_{}_KLD_{}_PT_{}_F_{}'.format(
            params['evn'],params['num roll in'],params['T'],params['imitation'], params['expert'],
            params['n_epoch'],params['im_lr'], params['po_lr'],params['KL Delta'], 
            params['Pre-Train'], params['filter']);

    #final_results = [[(expert_info[0],expert_info[1])], im_rr, po_rr];
'''