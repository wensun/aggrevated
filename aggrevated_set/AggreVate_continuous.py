import numpy as np
from IPython import embed
import gym
import keras
import theano
import theano.tensor as T 
from keras.models import load_model
from AggreVate import AggreVate_set
#from policy_continuous import Policy_continuous
import cPickle
import sys

class AggreVate_continuous(AggreVate_set):
    def __init__(self, env, policy_model, obs_dim, x_dim, a_dim, T = 100, 
                    num_roll_in = 10,
                    imitation_train_method = 'cost sensitive', 
                    policy_train_method = 'policy original', 
                    setup_filter = True,
                    scale = 1e-1, vr = True):
        
        AggreVate_set.__init__(self, env, policy_model, 
                        obs_dim, x_dim, a_dim, T,
                        num_roll_in, imitation_train_method,
                        policy_train_method, setup_filter, scale, vr = vr);
        
    def get_state(self):
        return self.env.state_vector();
    
    def set_state(self, state):
        self.env.set_state(state[:self.env.data.qpos.shape[0]],
                    state[self.env.data.qpos.shape[0]:]);
    
    def get_obs(self):
        obs = self.env._get_obs();
        return obs;

    def stack_obs_U_ctg_prob(self, trajs_x, trajs_a):
        for i in xrange(len(trajs_x)):
            valid_len = trajs_a[i].shape[0] - np.sum(trajs_a[i][:,0] == -np.inf);
            stacked_x = trajs_x[i][:valid_len] if i==0 else np.concatenate([stacked_x,trajs_x[i][:valid_len]],axis = 0);
        return stacked_x;

    def initialize_policy(self, n_epoch = 32, lr = 0.001):
        inputs_dic = self.rolling_in(
                expert = True, num_roll_in = self.num_of_roll_in, 
                collect_optimal_ctg = False);
        stacked_obs =self.stack_obs_U_ctg_prob(inputs_dic['list_obs'],
                                            inputs_dic['list_U']);
        stacked_a = self.stack_obs_U_ctg_prob(inputs_dic['list_U'],
                                            inputs_dic['list_U']);
        for i in xrange(n_epoch):
            output = self.policy.train_nll(stacked_obs, stacked_a, lr);
        return (np.mean(inputs_dic['list_true_c']), np.std(inputs_dic['list_true_c']));

'''
if __name__ == '__main__':
    #np.random.seed(100);
    if len(sys.argv) > 1:
        robot_name = str(sys.argv[1]);
        if robot_name == 'Hopper':
            openAI_model_name = robot_name + '-v1';
        elif robot_name == 'Walker':
            openAI_model_name = robot_name + '2d-v1';
        else:
            print "no such robot named {}".format(robot_name);
            assert False;

        Expert_name = robot_name + '_TRPO_agent.save';
        imitation_train_method = str(sys.argv[2]);
    else:
        openAI_model_name = 'Swimmer-v1';
        imitation_train_method = 'explicit GD L2'; #'explicit GD KL'  #'explicit GD L2'; #'implicit GD KL'; 
        Expert_name = 'Swimmer_TRPO_agent.save';


    env = gym.make(openAI_model_name);
    #env.seed(100);
    obs_dim = env.reset().shape[0];
    x_dim = env.state_vector().shape[0];
    a_dim = env.action_space.shape[0];
    num_roll_in = 50;
    T = 100;
    alpha = 1.0;
    policy_train_method = 'explicit GD L2';
    cg_iter = 20;
    reg = 0.#1e-2;

    vr = False; #whether or not to apply variance reduction technique: aka use advantage function .
    po_or_im = 'rl';

    params = {'num roll in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[64], 
    'evn':openAI_model_name, 'expert':Expert_name, 
    'n_epoch':1000, 'lr':1e-2, 'po_lr':1e-3,'KL Delta':1e-2*1., 
    'Agg_iter':100, 'Pre-Train':True,
    'filter':True, 'alpha': alpha,
    'scale':0.5, 'cg_iter':cg_iter, 'reg':reg, 'vr':vr};    
    print params;

    final_results = [];
    for repeat in xrange(5):
        env = gym.make(openAI_model_name);
        env.seed(repeat);
        np.random.seed(repeat);
        print "repeat {}".format(repeat);
        pi = Policy_continuous(dx = obs_dim, k = a_dim, 
                            num_layers=params['num hid Layer'], 
                            nh = params['nh'], 
                            num_roll_in = params['num roll in'],
                            cg_iter = params['cg_iter'], reg = params['reg']);
    
        Learner = AggreVate_continuous(env=env, policy_model = pi,
                    obs_dim = obs_dim, x_dim = x_dim, a_dim = a_dim, 
                    T = params['T'], 
                    num_roll_in = params['num roll in'], 
                    imitation_train_method = params['imitation'], 
                    policy_train_method = params['policy'],
                    setup_filter = params['filter'],
                    scale = params['scale'], vr = params['vr']);

        Learner.load_save_Expert(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 200, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);

        if po_or_im == 'im':
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                        pre_train=params['Pre-Train'], 
                        n_epoch = params['n_epoch'], lr = params['lr'], 
                        KL_Delta = params['KL Delta'], MC = False, 
                        alpha = params['alpha']); 
            exp_im = {'expert':(expert_info[0],expert_info[1]), 'im':im_rr}; 
        else:
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC=False);
            exp_im = {'expert':(expert_info[0],expert_info[1]), 'po':po_rr};
        
        final_results.append(exp_im);

    filername = 'result_Model_{}_numrollin_{}_T_{}_method_{}_expert_{}_n_epoch_{}_lr_{}_KLD_{}_PT_{}_F_{}'.format(
        params['evn'],params['num roll in'],params['T'],params['imitation'], params['expert'],
        params['n_epoch'],params['lr'], params['KL Delta'], params['Pre-Train'], params['filter'])
'''






