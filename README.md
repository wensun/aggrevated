This repository implements AggreVaTed: a Differentiable Imitation Learning Algorithm. 

Dependencies:

1. Keras (1.1.0)
2. Theano (0.8.2)
3. Open AI Gym (0.7.3) and MuJoCo 1.31 for Linux and MuJoCo License

To run the algorithm implemented here, you should put modular_rl from the holder modular_rl-master (downloaded and copied from John Schulman) on your PYTHONPATH

1. To run dependency parsing experiments:
    python test_parsing.py 'eager' 'explicit GD L2'

2. To run Discrete Action setting using CartPole and Acrobot from Open AI Gym (replace 'explicit GD L2' by 'explicit GD KL' for natural gradient, and replace 'CartPole' by 'Acrobot' for acrobot experiments): 
    python test_discrete.py 'CartPole' 'explicit GD L2'  
   
3. To run Acrobot Partial Observable Setting (replace 'explicit GD L2' by 'explicit GD KL' for natural gradient):
    python test_discrete_partial.py 'Acrobot' 'explicit GD L2'

4. To run Continuous Action setting using MuJoCo Hopper or Walker (replace 'explicit GD L2' by 'explicit GD KL' for natural gradient, replace Hopper by Walker for walker experiment):
    python test_continuous.py 'Hopper' 'explicit GD L2'




