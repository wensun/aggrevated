import numpy as np
from IPython import embed
import gym
import keras
import theano
import theano.tensor as T 
from aggrevated_set import AggreVate_continuous
from network_set import Policy_continuous
import cPickle
import sys


if __name__ == '__main__':
    #np.random.seed(100);
    if len(sys.argv) > 1:
        robot_name = str(sys.argv[1]);
        imitation_train_method = str(sys.argv[2]);
        #openAI_model_name = str(sys.argv[1]);
        #Expert_name = str(sys.argv[2]);
        #imitation_train_method = str(sys.argv[3]);
    else:
        openAI_model_name = 'Hopper-v1';
        imitation_train_method = 'explicit GD L2'; #'explicit GD KL'  #'explicit GD L2'; #'implicit GD KL'; 
        Expert_name = 'Hopper_TRPO_agent.save';
    
    if robot_name == 'Walker':
        openAI_model_name = 'Walker2d-v1';
        Expert_name = 'Walker_TRPO_agent.save';
        KL_delta = 1e1;
        im_lr = 1e-3; 
    elif robot_name == 'Hopper':
        openAI_model_name = 'Hopper-v1'
        Expert_name = 'Hopper_TRPO_agent.save';
        KL_delta = 1e0;
        im_lr = 1e-2;


    env = gym.make(openAI_model_name);
    #env.seed(100);
    obs_dim = env.reset().shape[0];
    x_dim = env.state_vector().shape[0];
    a_dim = env.action_space.shape[0];
    num_roll_in = 50;
    T = 100;
    alpha = 1.0;
    policy_train_method = 'explicit GD KL';
    cg_iter = 20;
    reg = 0.#1e-2;

    vr = False; #whether or not to apply variance reduction technique: aka use advantage function .
    po_or_im = 'im'; #do imitation learning.

    params = {'num roll in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[64], 
    'evn':openAI_model_name, 'expert':Expert_name, 
    'n_epoch':1000, 'lr':im_lr, 'po_lr':1e-3,'KL Delta':KL_delta, 
    'Agg_iter':100, 'Pre-Train':True,
    'filter':True, 'alpha': alpha,
    'scale':0.5, 'cg_iter':cg_iter, 'reg':reg, 'vr':vr};    
    print params;

    final_results = [];
    for repeat in xrange(5):
        env = gym.make(openAI_model_name);
        env.seed(repeat);
        np.random.seed(repeat);
        print "repeat {}".format(repeat);

        #construct the policy:
        pi = Policy_continuous(dx = obs_dim, k = a_dim, 
                            num_layers=params['num hid Layer'], 
                            nh = params['nh'], 
                            num_roll_in = params['num roll in'],
                            cg_iter = params['cg_iter'], reg = params['reg']);
    
        #construct AggreVaTeD learner.
        Learner = AggreVate_continuous(env=env, policy_model = pi,
                    obs_dim = obs_dim, x_dim = x_dim, a_dim = a_dim, 
                    T = params['T'], 
                    num_roll_in = params['num roll in'], 
                    imitation_train_method = params['imitation'], 
                    policy_train_method = params['policy'],
                    setup_filter = params['filter'],
                    scale = params['scale'], vr = params['vr']);

        Learner.load_save_Expert(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 200, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);

        if po_or_im == 'im':
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                        pre_train=params['Pre-Train'], 
                        n_epoch = params['n_epoch'], lr = params['lr'], 
                        KL_Delta = params['KL Delta'], MC = False, 
                        alpha = params['alpha']); 
            exp_im = {'expert':(expert_info[0],expert_info[1]), 'im':im_rr}; 
        else:
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC=False);
            exp_im = {'expert':(expert_info[0],expert_info[1]), 'po':po_rr};
        
        final_results.append(exp_im);

    filername = 'result_Model_{}_numrollin_{}_T_{}_method_{}_expert_{}_n_epoch_{}_lr_{}_KLD_{}_PT_{}_F_{}'.format(
        params['evn'],params['num roll in'],params['T'],params['imitation'], params['expert'],
        params['n_epoch'],params['lr'], params['KL Delta'], params['Pre-Train'], params['filter'])


#run test_continuous.py 'Hopper-v1' 'Hopper_TRPO_agent.save' 'explicit GD L2'