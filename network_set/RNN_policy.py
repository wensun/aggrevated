import numpy as np
import theano
import theano.tensor as T 
from SGD_opt import RMSProp
from SGD_opt import adam
from SGD_opt import natural_cg
from policy_set import Policy
from scipy import stats
import math


class LSTM_layer(object):
    def __init__(self, nh, input_dim, output_dim = None, id = 0):
        self.nh = nh;
        self.W_i = theano.shared(name='W_i',value= np.sqrt(2./(2*self.nh+input_dim))
                    *np.random.randn(self.nh, input_dim+self.nh).astype(theano.config.floatX));
        self.b_i = theano.shared(name='b_i',
                        value=np.zeros(self.nh).astype(theano.config.floatX));
        self.W_f = theano.shared(name='W_f',value= np.sqrt(2./(2*self.nh+input_dim))
                    *np.random.randn(self.nh,input_dim+self.nh).astype(theano.config.floatX));
        self.b_f = theano.shared(name='b_f',
                        value=np.zeros(self.nh).astype(theano.config.floatX));
        self.W_c = theano.shared(name='W_c',value= np.sqrt(2./(input_dim+2*self.nh))
                    *np.random.randn(self.nh,input_dim+self.nh).astype(theano.config.floatX));
        self.b_c = theano.shared(name='b_f',
                    value=np.zeros(self.nh).astype(theano.config.floatX));
        self.W_o = theano.shared(name='W_o',value= np.sqrt(2./(input_dim+2*self.nh))
                    *np.random.randn(self.nh,input_dim+self.nh).astype(theano.config.floatX));
        self.b_o = theano.shared(name='b_o',
                    value=np.zeros(self.nh).astype(theano.config.floatX));
        self.h_0 = theano.shared(name='h_0_{}'.format(id),
                    value=np.zeros(self.nh).astype(theano.config.floatX));
        self.c_0 = theano.shared(name='c_0_{}'.format(id),
                    value=np.zeros(self.nh).astype(theano.config.floatX));
        if id == 1:
            self.params = [self.W_i,self.b_i,self.W_f,self.b_f,self.W_c,self.b_c,
                            self.W_o,self.b_o,self.h_0,self.c_0];
        else:
            self.params = [self.W_i,self.b_i,self.W_f,self.b_f,self.W_c,self.b_c,
                            self.W_o,self.b_o];

        if output_dim is not None:
            self.W = theano.shared(name='W',value=np.sqrt(2./(output_dim+2*self.nh))
                        *np.random.randn(output_dim,2*self.nh).astype(theano.config.floatX));
            self.b = theano.shared(name='b',
                        value=np.zeros(output_dim).astype(theano.config.floatX));
            self.params.append(self.W);
            self.params.append(self.b);

    def update_h_c(self, input_x, h, c):
        f = T.nnet.sigmoid(T.dot(T.concatenate((h,input_x),axis=0),self.W_f.T) + self.b_f)#, alpha = 0.01);
        i = T.nnet.sigmoid(T.dot(T.concatenate((h,input_x),axis=0),self.W_i.T) + self.b_i)#, alpha = 0.01);
        tilde_c =   T.tanh(T.dot(T.concatenate((h,input_x),axis=0),self.W_c.T) + self.b_c);
        c_new = f*c + i*tilde_c;
        o = T.nnet.sigmoid(T.dot(T.concatenate((h,input_x),axis=0),self.W_o.T) + self.b_o)#, alpha = 0.01);
        h_new = o * T.tanh(c_new);
        return [h_new, c_new];

class LSTM_Policy(object):
    def __init__(self, x_dim_1, x_dim_2, a_dim, output_dim, nh, num_roll_in = 3, ridge = -1,
                    cg_iter = 10, reg = 1e-3):
        self.discrete = True;
        self.a_dim = a_dim;
        self.output_dim = output_dim;
        self.num_roll_in = num_roll_in;
        self.x_dim_1 = x_dim_1;
        self.x_dim_2 = x_dim_2;

        self.LSTM_1 = LSTM_layer(nh, input_dim = self.x_dim_1, output_dim = None, id = 1);
        self.LSTM_2 = LSTM_layer(nh, input_dim = self.x_dim_2, output_dim = output_dim, id = 2); 
        self.params = self.LSTM_1.params + self.LSTM_2.params;
        self.cg_iter = cg_iter;
        self.reg = reg;

        self.t_construct__X_Y_listX_listU_listC();
        self.construct_linear_vector_curr_params_nabla();
        self.construct_grad_nabla_updates();
        self.construct_functions();

        nll = -self.t_compute_log_likelihood(X1_list = self.list_traj_X1, X2_list = self.list_traj_X2,
                        U_list = self.list_traj_U);
        update_nll = RMSProp(nll, self.params, self.lr);
        #update_nll = adam(loss = nll, all_params = self.params, learning_rate=self.lr);
        self.train_nll = theano.function(inputs = [self.list_traj_X1,self.list_traj_X2,self.list_traj_U,self.lr],
                        outputs= nll, updates=update_nll, allow_input_downcast=True);
        
    def _t_LSTM_1_forward_pass(self, X):
        #valid_len = X.shape[0] - T.sum(X[:,0]< -1e10)
        [hs,cs],_ = theano.scan(fn = self.LSTM_1.update_h_c,
                    sequences=[X],outputs_info=[self.LSTM_1.h_0, self.LSTM_1.c_0],
                    n_steps=X.shape[0]);
        return [hs[-1], cs[-1]]; #return the last hidden state and memory cell.
    
    def _t_LSTM_2_single_step(self, x, h, c):
        [h_new,c_new] = self.LSTM_2.update_h_c(input_x = x, h = h, c = c);
        y = T.nnet.softmax(T.dot(T.concatenate((h_new,c_new),axis=0),self.LSTM_2.W.T)+self.LSTM_2.b)[0];
        return [h_new,c_new, y]; #y is conditioned on x, h, c
    
    def _t_LSTM_2_forward_pass(self, X, h_0, c_0):
        [hs,cs,ys],_ = theano.scan(fn = self._t_LSTM_2_single_step,
                    sequences=[X],outputs_info=[h_0,c_0,None],
                    n_steps=X.shape[0]);
        return ys; #probability vectors. 2d matrix;

    def _t_LSTM_1_LSTM_2_forward_pass(self, X1, X2): 
        [h,c] = self._t_LSTM_1_forward_pass(X = X1);
        ys = self._t_LSTM_2_forward_pass(X = X2, h_0 = h, c_0 = c);
        return ys;

    def t_compute_prob(self, X1, X2, U = None): #x->u
        prob_vecs = self._t_LSTM_1_LSTM_2_forward_pass(X1,X2);
        if U is None:
            return prob_vecs;
        else:
            return prob_vecs[T.arange(U.shape[0]), U];

    def _t_compute_log_likelihood_single_traj(self, X1, X2,U):
        valid_len_1 = X1.shape[0] - T.sum(X1[:,0]< -1e10);
        valid_len_2 = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X1[0:valid_len_1],X2[0:valid_len_2], U[0:valid_len_2]);
        return T.mean(T.log(probs));
    
    def t_compute_log_likelihood(self, X1_list, X2_list, U_list):
        lls,_ = theano.scan(fn = self._t_compute_log_likelihood_single_traj, 
                        sequences=[X1_list, X2_list, U_list]);
        return T.mean(lls);

    def _t_cost_sensitive_single_traj(self, X1, X2, Y):
        valid_len_1 = X1.shape[0] - T.sum(X1[:,0] < -1e10)
        valid_len_2 = Y.shape[0] - T.sum(Y[:,0] < -1e10);  #cost to go less than -1e10,#####
        prob_vecs = self.t_compute_prob(X1 = X1[0:valid_len_1], X2 = X2[0:valid_len_2]);
        avg_cost = T.mean(T.sum(prob_vecs * Y[0:valid_len_2], axis = 1));
        return avg_cost;

    def t_cost_sensitive(self, X1_list, X2_list, Y_list):
        avg_css,_ = theano.scan(fn = self._t_cost_sensitive_single_traj, 
                        sequences=[X1_list,X2_list,Y_list]);
        return T.mean(avg_css);

    def t_traj_log_likelihood(self, X1, X2, U):
        valid_len_1 = X1.shape[0] - T.sum(X1[:,0] < -1e10);
        valid_len_2 = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X1[0:valid_len_1],X2[0:valid_len_2]);
        ll = T.mean(T.log(probs)[T.arange(valid_len_2), U[0:valid_len_2]]);
        return ll;

    def _t_traj_cost_single_traj(self, X1, X2, U, Ctg):
        valid_len_1 = X1.shape[0] - T.sum(X1[:,0] < -1e10);
        valid_len_2 = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X1[0:valid_len_1],X2[0:valid_len_2], U[0:valid_len_2]);
        cost = T.mean(T.log(probs) * Ctg[0:valid_len_2]);
        return cost;
    
    def t_traj_cost(self, X1_list, X2_list, U_list, Ctg_list):
        cs,_ = theano.scan(fn = self._t_traj_cost_single_traj, 
                        sequences = [X1_list, X2_list, U_list, Ctg_list]);
        return T.mean(cs);

    def t_construct__X_Y_listX_listU_listC(self):   
        self.X = T.matrix('single_traj');
        self.obs = T.vector('single_obs');
        self.t_hid = T.vector('hidden');
        self.t_cell = T.vector('mem cell');
        symbolic_list_trajs_X1 = [];
        symbolic_list_trajs_X2 = []; #symbolic representation of a list of trajectories (states)
        symbolic_list_trajs_U = []; #simbolic representation of a list of controls (U)
        symbolic_list_trajs_Y = [];  
        symbolic_list_trajs_ctg = [];
        symbolic_list_costs = []; #simbolic representation of a list of trajectory accumulative cost. 
        for i in xrange(0, self.num_roll_in):
            symbolic_list_trajs_X1.append(T.matrix('trajx_{}'.format(i)));
            symbolic_list_trajs_X2.append(T.matrix('trajx_{}'.format(i)));
            symbolic_list_trajs_U.append(T.ivector('traju_{}'.format(i)));
            symbolic_list_trajs_Y.append(T.matrix('trajcs_{}'.format(i)));
            symbolic_list_trajs_ctg.append(T.vector('trajctg_{}'.format(i)));
            symbolic_list_costs.append(T.scalar('cost_{}'.format(i)));
        self.list_traj_X1 = T.stack(symbolic_list_trajs_X1);
        self.list_traj_X2 = T.stack(symbolic_list_trajs_X2);
        self.list_traj_U = T.stack(symbolic_list_trajs_U);
        self.list_traj_Y = T.stack(symbolic_list_trajs_Y);
        self.list_traj_ctg = T.stack(symbolic_list_trajs_ctg);
        self.list_cost = T.stack(symbolic_list_costs);

        self.lr = T.scalar('learning rate');
        self.KL_Delta = T.scalar('kl delta');

    def construct_linear_vector_curr_params_nabla(self):
        self.linear_vector_shared = theano.shared(name = 'linear_gradient', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.linear_vector = T.vector();
        self.curr_params_shared = theano.shared(name = 'vectorized_curr_params', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.curr_params = T.vector('vectorized_curr_params');
        self.nabla_tmp = T.matrix('nabla_tmp');
        self.nabla = theano.shared(name = 'nabla', value = np.zeros((self.num_roll_in, 
                self.linear_vector_shared.get_value().shape[0])).astype(theano.config.floatX));

    def construct_grad_nabla_updates(self):
        self.fn_update_curr_params = theano.function(inputs = [], outputs = [], 
                updates = [(self.curr_params_shared, T.concatenate([p.ravel() for p in self.params],axis=0))],
                allow_input_downcast=True);
        grad_mat,_ = theano.scan(fn = self.t_compute_gradient_traj_ll_vectorized, 
                                sequences = [self.list_traj_X1, self.list_traj_X2, self.list_traj_U]);
        self.fn_update_nabla = theano.function(inputs = [self.list_traj_X1,self.list_traj_X2,self.list_traj_U], 
                                outputs = [], 
                                updates = [(self.nabla, grad_mat)], allow_input_downcast = True);
    
    def t_compute_gradient_traj_ll_vectorized(self,X1,X2,U):
        ll = self.t_traj_log_likelihood(X1,X2,U);
        grad = T.grad(cost = ll, wrt = self.params);
        return T.concatenate([p.ravel() for p in grad], axis =0);
    
    def construct_functions(self):
        #initialize hidden state and cell for LSTM_2
        [h_2, c_2] = self._t_LSTM_1_forward_pass(X = self.X);
        self.initialize_LSTM_2_h_c = theano.function(inputs = [self.X], outputs = [h_2,c_2], allow_input_downcast = True);
        
        prob_vec = T.nnet.softmax(T.dot(T.concatenate((self.t_hid,self.t_cell),axis=0),self.LSTM_2.W.T)+self.LSTM_2.b)[0];
        self.predict_prob = theano.function(inputs = [self.t_hid,self.t_cell], outputs = prob_vec, allow_input_downcast=True);

        #update hidden state and mem cell:
        [h_new,c_new] = self.LSTM_2.update_h_c(input_x = self.obs , h = self.t_hid, c = self.t_cell);
        self.update_hid_cell = theano.function(inputs = [self.obs,self.t_hid,self.t_cell],outputs = [h_new,c_new],allow_input_downcast=True);

        self.cost_sen = self.t_cost_sensitive(X1_list = self.list_traj_X1, X2_list = self.list_traj_X2, Y_list = self.list_traj_Y);
        self.update_original_imitation = adam(self.cost_sen, self.params, self.lr);
        self.train_original_imitation = theano.function(
                    inputs = [self.list_traj_X1,self.list_traj_X2,self.list_traj_Y, self.lr], 
                    outputs = self.cost_sen, 
                    updates = self.update_original_imitation, allow_input_downcast = True);
        #natural update step:
        self.update_original_imitation_natural = natural_cg(self.cost_sen, self.params, self.nabla_tmp, 
                                        self.curr_params, self.KL_Delta, epsilon = self.reg, 
                                        M = self.num_roll_in, n_iter = self.cg_iter);
        self.train_imitation_natural = theano.function(inputs = [self.list_traj_X1,self.list_traj_X2,self.list_traj_Y,self.KL_Delta],
                            outputs=self.cost_sen,
                            updates = self.update_original_imitation_natural,
                            givens={self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared},
                            allow_input_downcast=True);

        #RL:
        self.traj_cost = self.t_traj_cost(self.list_traj_X1,self.list_traj_X2, self.list_traj_U, self.list_traj_ctg);
        self.update_original_policy = adam(self.traj_cost, self.params, self.lr);
        self.train_original_policy = theano.function(
                    inputs = [self.list_traj_X1,self.list_traj_X2, self.list_traj_U, self.list_traj_ctg, self.lr],
                    outputs = self.traj_cost, 
                    updates = self.update_original_policy, allow_input_downcast = True);  
        #natural update:
        self.update_original_policy_natural = natural_cg(self.traj_cost, self.params, self.nabla_tmp,
                                        self.curr_params, self.KL_Delta, epsilon = self.reg, M = self.num_roll_in, n_iter = self.cg_iter);
        self.train_policy_natural = theano.function(inputs = [self.list_traj_X1,self.list_traj_X2, self.list_traj_U, self.list_traj_ctg, self.KL_Delta],
                    outputs=self.traj_cost,
                    updates = self.update_original_policy_natural,
                    givens = {self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared}, 
                    allow_input_downcast = True);
        
    def reset(self, X):
        [self.hidden, self.cell_mem] = self.initialize_LSTM_2_h_c(X); #update the hidden state and mem for LSTM 2, using LSTM 1 to pass through traj X
        return np.concatenate((self.hidden,self.cell_mem),axis=0);
    
    def update_hidden_cell(self, x): #x:obs
        [self.hidden,self.cell_mem] = self.update_hid_cell(x,self.hidden,self.cell_mem);
        return np.concatenate((self.hidden,self.cell_mem),axis=0);

    def sample_action(self, x):
        self.update_hidden_cell(x);
        prob_vec = self.predict_prob(self.hidden,self.cell_mem);
        assert prob_vec.ndim == 1 and prob_vec.shape[0] == self.output_dim;
        if math.isnan(prob_vec[0]):
            print "generated a nan action....";
            #embed()
            prob_vec = np.ones(self.output_dim) / (self.output_dim*1.);
        xk = np.arange(self.output_dim);
        custm = stats.rv_discrete(name='custm', values=(xk, prob_vec));
        action = custm.rvs();
        return action, prob_vec[action];

    def fit_im_cost_sensitive(self, X1_list, X2_list, Y_list=None, lr = 0.01):
        #padding X1_list:
        len_max = np.max([a.shape[0] for a in X1_list]);
        tmp_X1_list = [None]*len(X1_list);

        output = self.train_original_imitation(X1_list, X2_list,Y_list, lr); 
        return output;
    
    def fit_im_natural_gradient(self, X1_list, X2_list, Y_list, KL_Delta):
        output = self.train_imitation_natural(X1_list,X2_list,Y_list, KL_Delta);
        return output;

    
    def fit_policy_gradient(self,  X1_list, X2_list, U_list, ctg_list, lr = 0.01):
        output = self.train_original_policy(X1_list,X2_list,U_list,ctg_list, lr);
        return output;

    def fit_policy_natural_gradient(self, X1_list,X2_list, U_list, ctg_list, KL_Delta = 0.01):
        output = self.train_policy_natural(X1_list,X2_list, U_list, ctg_list, KL_Delta);
        return output;

    
if __name__ == '__main__':
    pi = LSTM_Policy(x_dim_1 = 2, x_dim_2 = 2, a_dim = 3, output_dim = 3, nh = 5, num_roll_in = 3);
    pi.reset(X = np.random.rand(5,2));
    pi.sample_action(x = np.random.rand(2));
    
    



