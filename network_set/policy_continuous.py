import numpy as np
import theano
import theano.tensor as T
from SGD_opt import RMSProp
from SGD_opt import sgd
from SGD_opt import adam
from SGD_opt import natural_sgd
from SGD_opt import natural_sgd_naive
from IPython import embed
from scipy import stats
from policy_set import Policy

''' 
Continuous action space: 
'''        
class Policy_continuous(Policy):
    def __init__(self, dx, k, num_layers, nh, num_roll_in = 10, 
                cg_iter = 10, reg = 1e-3):
        Policy.__init__(self,dx, k, num_layers, nh, num_roll_in,cg_iter=cg_iter,
                    reg = reg);
        #add extra diagnoal params for (diagnoal) covariance matrix. std = exp(r).
        self.discrete = False;
        
        self.params[-2].set_value(self.params[-2].get_value() * 0.1); #decrease parameters to make sure the initial predicted actions are centered around zeros.
        self.r = theano.shared(name = 'diag params', 
                          value = -np.zeros(self.dy).astype(theano.config.floatX)); #this initliazation makes sure that the std (exp(r)) is around 0.3, so that 3-std is around 1, which is the max control input for mujoco setups. 
        self.params.append(self.r); 
        
        self.t_construct__X_Y_listX_listU_listC();
        self.construct_linear_vector_curr_params_nabla();
        self.construct_cost_sen_traj_cost();
        self.construct_loss_quadratic_dis();
        self.construct_updates();
        self.consctruct_train_procedures();
        self.construct_grad_nabla_updates();
            
        diag_vars = (T.exp(self.r))**2;
        pred_means = self.t_compute_mean(self.X_cs); 
        self.construct_normal_dis = theano.function(inputs = [self.X_cs],
                            outputs = [pred_means, diag_vars], allow_input_downcast=True);
        probs = self.t_compute_prob(self.X_cs, self.U_cs);
        lls = self.t_compute_log_liklihood(self.X_cs, self.U_cs);
        self.compute_probs = theano.function(inputs = [self.X_cs,self.U_cs], outputs = probs, allow_input_downcast=True);
        self.compute_log_likelihood = theano.function(inputs=[self.X_cs,self.U_cs], outputs=lls, 
                    allow_input_downcast=True);

        lr = T.scalar('nll_lr');
        nll = -T.mean(lls);
        update_nll = adam(loss = nll, all_params = self.params, learning_rate = lr);
        self.train_nll = theano.function(inputs=[self.X_cs,self.U_cs, lr], outputs = nll, 
                                    updates = update_nll, allow_input_downcast=True);
        
    def t_construct__X_Y_listX_listU_listC(self):
        self.X_cs = T.matrix('Sq_obs'); #s_i
        self.U_cs = T.matrix('Sq_act'); #a_i.
        self.Y_cs = T.vector('Sq_Q*'); #vector contains the Q(s_i, a_i) for all i.
        
        self.X = T.matrix('traj states');
        self.U = T.matrix('traj controls');
        self.Ctg = T.vector('traj ctgs');
        
        self.Prob_cs = T.vector('Sq_a_prob'); #vector contains pi_{theta_old}(a_t | s_t). 
        symbolic_list_trajs_X = []; #symbolic representation of a list of trajectories (states)
        symbolic_list_trajs_U = [];#simbolic representation of a list of controls (U)
        symbolic_list_costs = []; #simbolic representation of a list of trajectory accumulative cost. 
        for i in xrange(0, self.num_roll_in):
            symbolic_list_trajs_X.append(T.matrix('trajx_{}'.format(i)));
            symbolic_list_trajs_U.append(T.matrix('traju_{}'.format(i)));
            symbolic_list_costs.append(T.scalar('cost_{}'.format(i)));
        self.list_traj_X = T.stack(symbolic_list_trajs_X);
        self.list_traj_U = T.stack(symbolic_list_trajs_U);
        self.list_cost = T.stack(symbolic_list_costs);
    

    def t_compute_mean(self, X):
        input_X = X;
        output = X;
        for i in xrange(len(self.layers)):
            output = self.layers[i]._minibatch_forward(input_X);
            input_X = output;
        pred = T.dot(output, self.W_y.T) + self.b_y;
        return pred;

    def compute_single_m_dis(self, du, variances):
        #assert du.shape[0] == variances.shape[0];
        return 0.5*(1./variances * du).dot(du);

    def t_compute_prob(self, X, U):
        #compute diag variance:
        variances = (T.exp(self.r))**2; #std->var. 
        det = T.prod(variances);
        coeff = 1./T.power(T.power(2.*np.pi, self.dy)*det, 0.5);
        U_means = self.t_compute_mean(X);
        d_U = U - U_means;
        m_diss,_ = theano.scan(fn = self.compute_single_m_dis, 
                            sequences = [d_U], non_sequences=variances, n_steps=d_U.shape[0]);
        probs = coeff * T.exp(-m_diss);
        return probs; #a 1-d vector: each element represents pi(u_i | s_i).

    def t_compute_log_liklihood(self, X, U):
        variances = (T.exp(self.r))**2; #std->var. 
        det = T.prod(variances);
        U_means = self.t_compute_mean(X);
        d_U = U - U_means;
        m_diss,_ = theano.scan(fn = self.compute_single_m_dis, 
                            sequences = [d_U], non_sequences=variances, n_steps=d_U.shape[0]);
        ll = -0.5*(T.log(det) + self.dy*T.log(2*np.pi)) - m_diss;
        return ll;

    def t_cost_sensitive(self, X, Y, U, Probs):
        #X: 2d matrix, Y: 1-d vector (Q(s,a)), U: 2d matrix, Probs: 1-d vector;
        prob_pi = self.t_compute_prob(X, U);
        ratios = prob_pi / Probs;
        return T.mean(ratios * Y);

    def t_traj_log_likelihood(self, X, U):
        #X: 2d matrix; U: 2_d vector,
        valid_state_len = U.shape[0] - T.sum(U[:,0] <= -1e5)
        #valid_state_len = U.shape[0] - T.sum(T.eq(U_sum, 0));
        ll_s = self.t_compute_log_liklihood(X[:valid_state_len], 
                                            U[:valid_state_len]);
        ll = T.sum(ll_s);
        return ll;
    
    def sample_action(self, x):
        [means, diag_vars] = self.construct_normal_dis(x.reshape(1,-1));
        mean = means[0];
        #sample from the normal distribution:
        action = np.random.randn(self.dy) * (diag_vars**0.5) + mean;
        prob = self.compute_probs(x.reshape(1,-1), action.reshape(1,-1))[0];
        return action, prob;


if __name__ == '__main__':
    #pi = Policy_discrete(dx = 4, k = 3, num_layers = 1, nh = [32]);
    pi_c = Policy_continuous(dx = 4, k = 3, num_layers = 1, nh = [32]);

    x = np.random.rand(4);
    u = np.random.rand(3);
    prob = pi_c.compute_probs(x.reshape(1,-1), u.reshape(1,-1));
    ll = pi_c.compute_log_likelihood(x.reshape(1,-1), u.reshape(1,-1));

    [m, vs] = pi_c.construct_normal_dis(x.reshape(1,-1));
    m = m[0];
    l = 1.;
    for i in xrange(m.shape[0]):
        l = l * stats.norm(m[i], vs[i]**0.5).pdf(u[i]);
    print l;