import numpy as np
import theano
import theano.tensor as T
from SGD_opt import RMSProp
from SGD_opt import sgd
from SGD_opt import adam
from SGD_opt import natural_sgd
from SGD_opt import natural_sgd_naive
from IPython import embed
from scipy import stats
import math
from policy_set import Policy

class Policy_discrete(Policy):#finite actions. 
    def __init__(self, dx, k, num_layers, nh, num_roll_in = 10, ridge = -1.,
                cg_iter = 10, reg = 1e-3, tanh = False):
        Policy.__init__(self,dx, k, num_layers, nh, num_roll_in, ridge, cg_iter, reg, tanh);
        self.discrete = True;
        
        self.t_construct__X_Y_listX_listU_listC();
        self.construct_linear_vector_curr_params_nabla();
        self.construct_cost_sen_traj_cost();
        self.construct_loss_quadratic_dis();
        self.construct_updates();
        self.consctruct_train_procedures();
        self.construct_grad_nabla_updates();
        
        prob_vecs = self.t_compute_prob(self.X_cs);
        self.predict_prob = theano.function(inputs = [self.X_cs], 
                            outputs = prob_vecs, allow_input_downcast=True);

        nll = -self.t_compute_log_liklihood(self.X_cs, U = self.U);
        #update_nll = RMSProp(nll, self.params, self.lr);
        update_nll = adam(loss = nll, all_params=self.params,learning_rate=self.lr);
        self.train_nll = theano.function(inputs=[self.X_cs,self.U, self.lr], outputs = nll, 
                                    updates = update_nll, allow_input_downcast=True);

    def t_construct__X_Y_listX_listU_listC(self):
        self.X_cs = T.matrix('cs states');
        self.Y_cs = T.matrix('cs ctg vector'); #each row is a cost-to-go vector,
        
        self.X = T.matrix('traj states');
        self.U = T.ivector('traj controls');
        self.Ctg = T.vector('traj ctgs');
        
        symbolic_list_trajs_X = []; #symbolic representation of a list of trajectories (states)
        symbolic_list_trajs_U = [];#simbolic representation of a list of controls (U)
        symbolic_list_costs = []; #simbolic representation of a list of trajectory accumulative cost. 
        for i in xrange(0, self.num_roll_in):
            symbolic_list_trajs_X.append(T.matrix('trajx_{}'.format(i)));
            symbolic_list_trajs_U.append(T.ivector('traju_{}'.format(i)));
            symbolic_list_costs.append(T.scalar('cost_{}'.format(i)));
        self.list_traj_X = T.stack(symbolic_list_trajs_X);
        self.list_traj_U = T.stack(symbolic_list_trajs_U);
        self.list_cost = T.stack(symbolic_list_costs);
    
    def t_compute_prob(self, X, U = None):
        input_X = X;
        output = X;
        for i in xrange(len(self.layers)):
            output = self.layers[i]._minibatch_forward(input_X);
            input_X = output;
        pred = T.dot(output, self.W_y.T) + self.b_y;
        prob_vecs = T.nnet.softmax(pred);
        if U is None:
            return prob_vecs;
        else:
            return prob_vecs[T.arange(U.shape[0]), U];

    def t_compute_log_liklihood(self, X, U):
        probs = self.t_compute_prob(X, U);
        ll = T.mean(T.log(probs)); #the log-likelihood of the U being generated from X.
        return ll;

    def t_cost_sensitive(self, X, Y): #X: 2d matrix, Y:2dmatrix
        prob_vecs = self.t_compute_prob(X);
        avg_cost = T.mean(T.sum(prob_vecs * Y, axis = 1));
        
        if self.ridge > 0.0: #add regularization:
            for i in xrange(len(self.params)): #only penalize the weight matrix W,
                avg_cost = avg_cost + self.ridge * T.sum(self.params[i]**2);
        
        return avg_cost; 
    
    def t_traj_log_likelihood(self, X, U):
        #X: 2d matrix; U: 1_d vector, 
        valid_state_len = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X);
        ll = T.mean(T.log(probs)[T.arange(valid_state_len), U[0:valid_state_len]]);
        return ll;
    
    def sample_action(self, x):
        prob_vec = self.predict_prob(x.reshape(1,-1))[0];
        assert prob_vec.ndim == 1 and prob_vec.shape[0] == self.dy;
        if math.isnan(prob_vec[0]):
            print "generated a nan action....";
            #embed()
            prob_vec = np.ones(self.dy) / (self.dy*1.);
        xk = np.arange(self.dy);
        custm = stats.rv_discrete(name='custm', values=(xk, prob_vec));
        action = custm.rvs();
        return action, prob_vec[action];


if __name__ == '__main__':
    pi = Policy_discrete(dx = 4, k = 3, num_layers = 1, nh = [32]);