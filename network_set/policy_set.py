#from FeedForward_NN import FeedForward_NN_classifer
import numpy as np
import theano
import theano.tensor as T
from SGD_opt import RMSProp
from SGD_opt import sgd
from SGD_opt import adam
from SGD_opt import natural_sgd
from SGD_opt import natural_sgd_naive
from SGD_opt import natural_cg
from IPython import embed
from scipy import stats

#########################Single Layer class #########################
class Layer(object):
    '''
    x --> Linear --> LN --> Non-Linear -->output.
    y = sigma or Relu or Tanh (Layer_norm(Wx + b))
    '''
    def __init__(self, d_in, d_out, layer_id = 0, tanh = False):
        self.d_in = d_in;
        self.d_out = d_out;
        self.tanh = tanh;
        #self.nh = nh;
        self.layer_id = layer_id;
        name_W = 'layer_id_{}_W'.format(layer_id);
        name_b = 'layer_id_{}_b'.format(layer_id);
        self.W = theano.shared(name = name_W, 
                value = np.sqrt(2./(self.d_in + self.d_out))
                *np.random.randn(self.d_out, self.d_in).astype(theano.config.floatX));
        self.b = theano.shared(name = name_b, 
                    value = np.zeros(self.d_out).astype(theano.config.floatX));
        self.params = [self.W, self.b];

    def _minibatch_forward(self, X):
        Linear_output = T.dot(X, self.W.T) + self.b; #num of samples X d_out. 
        non_linear_output = T.nnet.relu(Linear_output, alpha = 0.01) if self.tanh is False else T.tanh(Linear_output);
        return non_linear_output; 

class Policy(object):
    def __init__(self, dx, k, num_layers, nh, num_roll_in = 3, ridge = -1.,
                    cg_iter = 10, reg = 1e-3, tanh = False):
        self.num_roll_in = num_roll_in;
        self.dx = dx;
        self.dy = k;
        self.num_layers = num_layers;
        self.params = [];
        self.layers = [];
        dim_in = self.dx;
        self.ridge = ridge;
        
        for i in xrange(self.num_layers):
            layer = Layer(dim_in, nh[i], i, tanh = tanh);
            dim_in = nh[i]; 
            self.params = self.params + layer.params;
            self.layers.append(layer);
        
        self.W_y = theano.shared(name = 'W_y', value = np.sqrt(2./(dim_in+self.dy))
                *np.random.randn(self.dy, dim_in).astype(theano.config.floatX));
        self.b_y = theano.shared(name = 'b_y', 
                    value = np.zeros(self.dy).astype(theano.config.floatX));
        self.params.append(self.W_y);
        self.params.append(self.b_y);

        #####################################construct symbolic variables. ##################################
        self.lr = T.scalar(name = 'sgd_learning_rate');
        self.KL_Delta = T.scalar(name = 'KL_Delta');
        self.mu = T.scalar(name = 'regularization coefficient for bragmen divergence');
        self.cg_iter = cg_iter;
        self.reg = reg;


    def t_construct__X_Y_listX_listU_listC(self):
        raise NotImplementedError;
    def t_cost_sensitive(self, X, Y, U = None, Probs = None):
        raise NotImplementedError;
    def t_compute_prob(self, X, U = None):
        raise NotImplementedError; 
    def t_traj_log_likelihood(self, X, U):
        raise NotImplementedError;
    def sample_action(self, X): #discrete or real value.
        raise NotImplementedError;
    def reset(self):
        pass;

    def construct_linear_vector_curr_params_nabla(self):
        self.linear_vector_shared = theano.shared(name = 'linear_gradient', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.linear_vector = T.vector();
        self.curr_params_shared = theano.shared(name = 'vectorized_curr_params', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.curr_params = T.vector('vectorized_curr_params');
        self.nabla_tmp = T.matrix('nabla_tmp');
        self.nabla = theano.shared(name = 'nabla', value = np.zeros((self.num_roll_in, 
                self.linear_vector_shared.get_value().shape[0])).astype(theano.config.floatX));

    def construct_cost_sen_traj_cost(self):
        if self.discrete == True:
            self.cost_sen = (1./self.num_roll_in)*self.t_cost_sensitive(
                        self.X_cs, self.Y_cs); #this is imitation learning loss function (per trajectory, hence there is a (1/num_roll_in) at the beginning). 
        elif self.discrete == False:
            self.cost_sen = (1./self.num_roll_in)*self.t_cost_sensitive(
                        self.X_cs, self.Y_cs, self.U_cs, self.Prob_cs);
        
        self.traj_cost = self.t_traj_cost(X=self.X, U = self.U, Ctg = self.Ctg);
    
    def construct_loss_quadratic_dis(self):
        [self.imitation_proximal_loss, 
                self.im_quadratic_dis]=self.t_original_loss_with_fisher_matrix(
                    self.cost_sen, self.curr_params, self.nabla_tmp, self.mu);#cost_sen(w) + 0.5/mu (w-w_t)^Tnabla^Tnabla(w-w_t).
        [self.policy_proximal_loss, 
                self.po_quadratic_dis]=self.t_original_loss_with_fisher_matrix(
                    self.traj_cost, self.curr_params, self.nabla_tmp, self.mu);#
    
    def construct_updates(self):
        #priginal update: no fisher information matrix involved:
        self.update_original_imitation = adam(self.cost_sen, self.params, self.lr);
        self.update_original_policy =  adam(self.traj_cost, self.params, self.lr);
        self.update_proximal_imitation = adam(self.imitation_proximal_loss, self.params, self.lr, epsilon=1e-8);
        #RMSProp(self.imitation_proximal_loss,self.params, self.lr); 
        #adam(self.imitation_proximal_loss, self.params, self.lr, epsilon=1e-5); 
        #sgd(self.imitation_proximal_loss, self.params, self.lr) 
        #adam(self.imitation_proximal_loss, self.params, self.lr);
        self.update_proximal_policy = adam(self.policy_proximal_loss, self.params, self.lr);
        
        self.update_original_imitation_natural = natural_cg(self.cost_sen,
                                    self.params, self.nabla_tmp,
                                    self.curr_params, self.KL_Delta, 
                                    epsilon = self.reg, M = self.num_roll_in, 
                                    n_iter = self.cg_iter);
        #self.update_original_imitation_natural = natural_sgd_naive(self.cost_sen,
        #                            self.params,self.nabla_tmp,
        #                            self.curr_params,self.KL_Delta,
        #                            epsilon = self.reg, M = self.num_roll_in);

        #self.update_original_imitation_natural = natural_sgd(self.cost_sen, 
        #                            self.params, self.nabla_tmp, 
        #                            self.curr_params, self.KL_Delta, 
        #                            epsilon = self.reg, M = self.num_roll_in);  #sgd(cost_sen, self.params, lr);
        
        self.update_original_policy_natural = natural_sgd(self.traj_cost, 
                                    self.params, self.nabla_tmp, 
                                    self.curr_params, self.KL_Delta, 
                                    epsilon = 1e-2, M = self.num_roll_in); 

    def consctruct_train_procedures(self):
        ####################construct training function (gradient descent):##################################
        if self.discrete == True:
            im_inputs_cost_sensitive = [self.X_cs, self.Y_cs];
        elif self.discrete == False:
            im_inputs_cost_sensitive = [self.X_cs, self.Y_cs, self.U_cs, self.Prob_cs];

        self.train_original_imitation = theano.function(
                    inputs = im_inputs_cost_sensitive + [self.lr], 
                    outputs = self.cost_sen, 
                    updates = self.update_original_imitation, allow_input_downcast = True);
        self.train_original_policy = theano.function(
                    inputs = [self.X,self.U,self.Ctg, self.lr],
                    outputs = self.traj_cost, 
                    updates = self.update_original_policy, allow_input_downcast = True);  
        self.train_imitation_proximal_loss=theano.function(
                    inputs= im_inputs_cost_sensitive + [self.mu, self.lr],
                    outputs=[self.imitation_proximal_loss, self.im_quadratic_dis],
                    updates = self.update_proximal_imitation,
                    givens = {self.curr_params:self.curr_params_shared, self.nabla_tmp:self.nabla}, 
                    allow_input_downcast=True);
        self.train_policy_proximal_loss=theano.function(
                    inputs=[self.X,self.U,self.Ctg, self.mu,self.lr],
                    outputs=[self.policy_proximal_loss,self.po_quadratic_dis],
                    updates = self.update_proximal_policy,
                    givens = {self.curr_params:self.curr_params_shared, self.nabla_tmp:self.nabla},
                    allow_input_downcast=True);

        self.train_imitation_natural = theano.function(
                    inputs = im_inputs_cost_sensitive + [self.KL_Delta], 
                    outputs = self.cost_sen,
                    updates=self.update_original_imitation_natural,
                    givens = {self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared},
                    allow_input_downcast=True);
        self.train_policy_natural = theano.function(
                    inputs = [self.X, self.U,self.Ctg,self.KL_Delta],
                    outputs=self.traj_cost,
                    updates=self.update_original_policy_natural,
                    givens = {self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared},
                    allow_input_downcast=True);


    def construct_grad_nabla_updates(self):
        if self.discrete == True:
            im_inputs_cost_sensitive = [self.X_cs, self.Y_cs];
        elif self.discrete == False:
            im_inputs_cost_sensitive = [self.X_cs, self.Y_cs, self.U_cs, self.Prob_cs];
        grads_cost_sen = T.grad(cost = self.cost_sen, wrt = self.params);
        self.fn_assign_grad_cs_to_linearvector= theano.function(inputs = im_inputs_cost_sensitive, 
                outputs = [],
                updates = [(self.linear_vector_shared, T.concatenate([p.ravel() for p in grads_cost_sen],axis = 0))], 
                allow_input_downcast=True);
        
        self.fn_update_curr_params = theano.function(inputs = [], outputs = [], 
                updates = [(self.curr_params_shared, T.concatenate([p.ravel() for p in self.params],axis=0))],
                allow_input_downcast=True);
        grad_mat,_ = theano.scan(fn = self.t_compute_gradient_traj_ll_vectorized, 
                                sequences = [self.list_traj_X, self.list_traj_U]);
        self.fn_update_nabla = theano.function(inputs = [self.list_traj_X,self.list_traj_U], 
                                outputs = [], 
                                updates = [(self.nabla, grad_mat)], allow_input_downcast = True);
        
    def t_compute_gradient_traj_ll(self, X, U):
        ll = self.t_traj_log_likelihood(X, U);
        grad = T.grad(cost = ll, wrt = self.params);
        return grad; 

    def t_compute_gradient_traj_ll_vectorized(self,X,U):
        grad = self.t_compute_gradient_traj_ll(X,U);
        return T.concatenate([p.ravel() for p in grad], axis =0);
    
    #def t_compute_single_traj_cost(self,X, U, cost):
    #        return self.t_traj_log_likelihood(X,U) * cost;
    
    def t_traj_cost(self, X, U, Ctg):
        #X: 2d matrix. 
        #U: 1d vector or 2d matrix;
        #Ctg: 1d vector. 
        probs = self.t_compute_prob(X=X, U= U);
        cost = T.mean(T.log(probs) * Ctg);
        
        if self.ridge > 0.0: #add regularization. 
            for i in xrange(len(self.params)):
                cost = cost + self.ridge * T.sum(self.params[i]**2);
        return cost;


    #def t_total_traj_cost(self, Xs, Us, costs): 
        #X is expected to be a list of matrix: in each matrix, each row stands for the state.
        #U is expected to be a list of  vector, in each vector, each entry is the action, which is resulting from the corresponding state.
        #total_reward is a list of scalar, each entry corresponds to the cumulative cost of the traj.
    #    values,_ = theano.scan(fn = self.t_compute_single_traj_cost, sequences=[Xs,Us,costs],n_steps=Xs.shape[0])
    #    return T.mean(values); #the traj log likelihood (per trajectory).

    #don't linear the loss, this correponds to implicit gradient-based approach:
    def t_original_loss_with_fisher_matrix(self, original_loss, old_params, nabla, mu):
        flatten_diff = T.concatenate([p.ravel() for p in self.params], axis = 0) - old_params;
        nabla_diff = nabla.dot(flatten_diff);
        penalty = (1./self.num_roll_in)*(nabla_diff.dot(nabla_diff) + 1e-3*flatten_diff.dot(flatten_diff)); 
        cost = original_loss + (0.5/mu) * penalty; 
        return cost, penalty;

    def fit_im_cost_sensitive(self, X = None, Y = None, U = None, 
                Probs = None, n_epoch = 1, lr = 0.01):
        flag = False;
        for e in xrange(n_epoch):
            if self.discrete == True:
                output = self.train_original_imitation(X, Y, lr); 
            elif self.discrete == False:
                output = self.train_original_imitation(X, Y, U, Probs,lr);
            #print output;
        return {'loss':output, 'flag':None};
    
    def fit_im_cost_sensitive_proximal(self, X = None, Y = None, 
                    U = None, Probs = None, 
                    n_epoch = 50, lr = 0.01, mu = 1e-1, KL_Delta = 0.1):
        flag = False;
        for e in xrange(n_epoch):
            if self.discrete == True:
                [output, q_dis] = self.train_imitation_proximal_loss(X,Y, mu, lr); #mu = 1/iter_id.
            elif self.discrete == False:
                [output, q_dis] = self.train_imitation_proximal_loss(X,Y, U, Probs, mu, lr); #mu = 1/iter_id.
            if q_dis > KL_Delta:
                flag = True;
                #print "break at iteration {}".format(e);
                #break;
            if np.mod(e, 50) == 0: 
                print "[implicitly solving Proximal] loss at epoch {} is {}".format(e, output);
        return {'loss':output, 'flag':flag};
    
    def fit_im_natural_gradient(self, X = None, Y = None, 
                        U = None, Probs = None, 
                        n_epoch = 1, lr = 0.01, mu = 1e-1, KL_Delta = 0.1):
        flag = False;
        for e in xrange(n_epoch):
            if self.discrete == True:
                #embed()
                output = self.train_imitation_natural(X,Y, KL_Delta);
            else:
                output = self.train_imitation_natural(X,Y,U,Probs,KL_Delta);
        return {'loss':output, 'flag':None};

    #inputs are lists. 
    def fit_policy_gradient(self, X = None, U = None, ctgs = None, lr = 0.01):
        flag = False;
        assert ctgs is not None;
        #print lr
        output = self.train_original_policy(X, U, ctgs, lr);
        #print output;
        return {'loss':output, 'flag':None};
    
    def fit_policy_proximal(self, X = None, U = None, ctgs = None, 
                    n_epoch = 50, lr = 0.01, mu = 1e-1, KL_Delta = 0.1):
        flag = False;
        for e in xrange(n_epoch):
            [output, q_dis] = self.train_policy_proximal_loss(X,U,ctgs, mu, lr);
            if q_dis > KL_Delta:
                flag = True;
                break;
            if np.mod(e, 5) == 0: 
                print "loss at epoch {} is {}".format(e, output);
        return {'loss':output, 'flag':flag};
    
    def fit_policy_natural_gradient(self, X = None, U = None, ctgs = None, 
                    n_epoch = 50, KL_Delta = 0.1):
        flag = False;
        for e in xrange(n_epoch):
            output = self.train_policy_natural(X,U,ctgs, KL_Delta);
        return {'loss':output, 'flag':None};
            

