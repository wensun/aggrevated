import numpy as np
import theano
import theano.tensor as T
from RNN_policy import LSTM_layer
from RNN_policy import LSTM_Policy
from IPython import embed
from SGD_opt import adam
from SGD_opt import natural_cg
from scipy import stats
import math


class LSTM_based_Policy(object):

    def __init__(self, x_dim, a_dim, output_dim, nh, num_roll_in = 3, cg_iter = 10, reg = 1e-3,
                    discrete = True):
        self.discrete = True;
        self.a_dim = a_dim;
        self.output_dim = output_dim;
        self.num_roll_in = num_roll_in;
        self.x_dim = x_dim;

        if discrete is True:
            self.discrete = discrete; #current doesnt' support continuous yet.
        else:
            print "current doesn't support continuous action yet. ";
            assert False;

        self.LSTM = LSTM_layer(nh, input_dim = self.x_dim, output_dim = self.output_dim, id = 1);
        self.params = self.LSTM.params;
        self.cg_iter = cg_iter;
        self.reg = reg;

        self.t_construct__X_Y_listX_listU_listC();
        self.construct_linear_vector_curr_params_nabla();
        self.construct_grad_nabla_updates();
        self.construct_functions();
    
    def _t_LSTM_single_step(self, x, h, c):
        [h_new,c_new] = self.LSTM.update_h_c(input_x = x, h = h, c = c);
        y = T.nnet.softmax(T.dot(T.concatenate((h_new,c_new),axis=0),self.LSTM.W.T)+self.LSTM.b)[0];
        return [h_new,c_new, y]; #y is conditioned on x, h, c
    
    def _t_LSTM_forward_pass(self, X):
        [hs,cs, ys],_ = theano.scan(fn = self._t_LSTM_single_step, 
                    sequences = [X], outputs_info=[self.LSTM.h_0,self.LSTM.c_0, None],
                    n_steps = X.shape[0]);
        return ys;
    
    def t_compute_prob(self, X, U = None):
        prob_vecs = self._t_LSTM_forward_pass(X = X);
        if U is None:
            return prob_vecs;
        else:
            return prob_vecs[T.arange(U.shape[0]), U];

    def _t_compute_log_likelihood_single_traj(self, X,U):
        valid_len_1 = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X[0:valid_len_1], U[0:valid_len_1]);
        return T.mean(T.log(probs));

    def t_compute_log_likelihood(self, X_list, U_list):
        lls,_ = theano.scan(fn = self._t_compute_log_likelihood_single_traj,
                    sequences = [X_list, U_list]);
        return T.mean(lls);
    
    def _t_cost_sensitive_single_traj(self, X, Y):
        valid_len = Y.shape[0] - T.sum(Y[:,0] <= -1e10);
        prob_vecs = self.t_compute_prob(X[:valid_len]);
        avg_cost = T.mean(T.sum(prob_vecs * Y[0:valid_len], axis= 1));
        return avg_cost;
    
    def t_cost_sensitive(self, X_list, Y_list):
        avg_css,_ = theano.scan(fn = self._t_cost_sensitive_single_traj,
                    sequences = [X_list, Y_list]);
        return T.mean(avg_css);
    
    def t_traj_log_likelihood(self, X, U):
        valid_len = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X[:valid_len],U[0:valid_len]);
        ll = T.mean(T.log(probs));
        return ll;
    
    def _t_traj_cost_single_traj(self, X, U, Ctg):
        valid_len = U.shape[0] - T.sum(U <= -1.);
        probs = self.t_compute_prob(X[:valid_len],U[:valid_len]);
        cost = T.mean(T.log(probs) * Ctg[:valid_len]);
        return cost;
    
    def t_traj_cost(self, X_list, U_list, Ctg_list):
        cs,_ = theano.scan(fn = self._t_traj_cost_single_traj, 
                sequences = [X_list, U_list, Ctg_list]);
        return T.mean(cs);

    def t_construct__X_Y_listX_listU_listC(self):   
        self.X = T.matrix('single_traj');
        self.obs = T.vector('single_obs');
        self.t_hid = T.vector('hidden');
        self.t_cell = T.vector('mem cell');
        symbolic_list_trajs_X = []; #symbolic representation of a list of trajectories (states)
        symbolic_list_trajs_U = []; #simbolic representation of a list of controls (U)
        symbolic_list_trajs_Y = [];  
        symbolic_list_trajs_ctg = [];
        symbolic_list_costs = []; #simbolic representation of a list of trajectory accumulative cost. 
        for i in xrange(0, self.num_roll_in):
            symbolic_list_trajs_X.append(T.matrix('trajx_{}'.format(i)));
            symbolic_list_trajs_U.append(T.ivector('traju_{}'.format(i)));
            symbolic_list_trajs_Y.append(T.matrix('trajcs_{}'.format(i)));
            symbolic_list_trajs_ctg.append(T.vector('trajctg_{}'.format(i)));
            symbolic_list_costs.append(T.scalar('cost_{}'.format(i)));
        self.list_traj_X = T.stack(symbolic_list_trajs_X);
        self.list_traj_U = T.stack(symbolic_list_trajs_U);
        self.list_traj_Y = T.stack(symbolic_list_trajs_Y);
        self.list_traj_ctg = T.stack(symbolic_list_trajs_ctg);
        self.list_cost = T.stack(symbolic_list_costs);

        self.lr = T.scalar('learning rate');
        self.KL_Delta = T.scalar('kl delta');
    
    def construct_linear_vector_curr_params_nabla(self):
        self.linear_vector_shared = theano.shared(name = 'linear_gradient', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.linear_vector = T.vector();
        self.curr_params_shared = theano.shared(name = 'vectorized_curr_params', 
                value = np.concatenate([p.get_value().ravel() for p in self.params],axis = 0).astype(theano.config.floatX));
        self.curr_params = T.vector('vectorized_curr_params');
        self.nabla_tmp = T.matrix('nabla_tmp');
        self.nabla = theano.shared(name = 'nabla', value = np.zeros((self.num_roll_in, 
                self.linear_vector_shared.get_value().shape[0])).astype(theano.config.floatX));
    
    def construct_grad_nabla_updates(self):
        self.fn_update_curr_params = theano.function(inputs = [], outputs = [], 
                updates = [(self.curr_params_shared, T.concatenate([p.ravel() for p in self.params],axis=0))],
                allow_input_downcast=True);
        grad_mat,_ = theano.scan(fn = self.t_compute_gradient_traj_ll_vectorized, sequences = [self.list_traj_X, self.list_traj_U]);
        self.fn_update_nabla = theano.function(
                    inputs = [self.list_traj_X, self.list_traj_U], 
                    outputs = [], 
                    updates = [(self.nabla, grad_mat)], allow_input_downcast = True);
    
    def t_compute_gradient_traj_ll_vectorized(self,X,U):
        ll = self.t_traj_log_likelihood(X,U);
        grad = T.grad(cost = ll, wrt = self.params);
        return T.concatenate([p.ravel() for p in grad], axis =0);
    
    def construct_functions(self):
        prob_vec = T.nnet.softmax(T.dot(T.concatenate((self.t_hid,self.t_cell),axis=0),self.LSTM.W.T)+self.LSTM.b)[0];
        self.predict_prob = theano.function(inputs = [self.t_hid,self.t_cell], outputs = prob_vec, 
                    allow_input_downcast=True);

        #update hidden state and mem cell:
        [h_new,c_new] = self.LSTM.update_h_c(input_x = self.obs , h = self.t_hid, c = self.t_cell);
        self.update_hid_cell = theano.function(inputs = [self.obs,self.t_hid,self.t_cell],outputs = [h_new,c_new],
                    allow_input_downcast=True);
        
        self.cost_sen = self.t_cost_sensitive(X_list = self.list_traj_X, Y_list = self.list_traj_Y);
        self.update_original_imitation = adam(self.cost_sen, self.params, self.lr);
        self.train_original_imitation = theano.function(
                    inputs = [self.list_traj_X,self.list_traj_Y, self.lr], 
                    outputs = self.cost_sen, 
                    updates = self.update_original_imitation, allow_input_downcast = True);
        #natural update step:
        self.update_original_imitation_natural = natural_cg(self.cost_sen, self.params, self.nabla_tmp, 
                                        self.curr_params, self.KL_Delta, epsilon = self.reg, 
                                        M = self.num_roll_in, n_iter = self.cg_iter);
        self.train_imitation_natural = theano.function(inputs = [self.list_traj_X,self.list_traj_Y,self.KL_Delta],
                            outputs=self.cost_sen,
                            updates = self.update_original_imitation_natural,
                            givens={self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared},
                            allow_input_downcast=True);
        #RL:
        self.traj_cost = self.t_traj_cost(self.list_traj_X, self.list_traj_U, self.list_traj_ctg);
        self.update_original_policy = adam(self.traj_cost, self.params, self.lr);
        self.train_original_policy = theano.function(
                    inputs = [self.list_traj_X, self.list_traj_U, self.list_traj_ctg, self.lr],
                    outputs = self.traj_cost, 
                    updates = self.update_original_policy, allow_input_downcast = True);  
        #natural update:
        self.update_original_policy_natural = natural_cg(self.traj_cost, self.params, self.nabla_tmp,
                                        self.curr_params, self.KL_Delta, epsilon = self.reg, M = self.num_roll_in, n_iter = self.cg_iter);
        self.train_policy_natural = theano.function(inputs = [self.list_traj_X, self.list_traj_U, self.list_traj_ctg, self.KL_Delta],
                    outputs=self.traj_cost,
                    updates = self.update_original_policy_natural,
                    givens = {self.nabla_tmp:self.nabla, self.curr_params:self.curr_params_shared}, 
                    allow_input_downcast = True);

    def reset(self):
        self.hidden = self.LSTM.h_0.get_value();
        self.cell_mem = self.LSTM.c_0.get_value();
        return np.concatenate((self.hidden,self.cell_mem),axis=0);
    
    def update_hidden_cell(self,x):
        [self.hidden,self.cell_mem] = self.update_hid_cell(x,self.hidden,self.cell_mem);
        return np.concatenate((self.hidden,self.cell_mem),axis=0);
    
    def sample_action(self,x):
        self.update_hidden_cell(x); #incoporate the latest obseration.
        prob_vec = self.predict_prob(self.hidden,self.cell_mem);
        xk = np.arange(self.output_dim);
        custm = stats.rv_discrete(name='custm', values=(xk, prob_vec));
        action = custm.rvs();
        return action, prob_vec[action];
    
    def fit_im_cost_sensitive(self, X_list, Y_list, lr):
        output = self.train_original_imitation(X_list, Y_list, lr);
        return output;
    
    def fit_im_natural_gradient(self, X_list, Y_list, KL_Delta):
        output = self.train_imitation_natural(X_list, Y_list, KL_Delta);
        return output;
    
    def fit_policy_gradient(self, X_list, U_list, ctg_list, lr):
        output = self.train_original_policy(X_list, U_list, ctg_list, lr);
        return output;
    
    def fit_policy_natural_gradient(self, X_list, U_list, ctg_list, KL_Delta):
        output = self.train_policy_natural(X_list, U_list, ctg_list, KL_Delta);
        return output;
        

if __name__ == '__main__':
    pi = LSTM_based_Policy(x_dim = 2, a_dim=2, output_dim = 2, nh =5, num_roll_in = 3);