import numpy as np 
import copy
from simpleFeatureSelector import simpleFeatureSelector
from IPython import embed
import cPickle

class configuration(object):
    '''
    define the class for the configuration, 
    '''
    def __init__(self):
        #for ROOT: we use feature zeros, and index 0,  
        self.dimFeats = 0;
        self.bufferSym = [];
        self.stackSym = []; ###note!!!! stack needs to start with Root.
        self.arcsSym = [];
        self.bufferIndex = [];
        self.stackIndex = [0]; #always start from 0, under whatever circumenstances. 
        self.arcsIndex = []; #always start with empty. 

    def initilize_cnfg(self, featureTS): 
        #featureTS: sequence of transitions represented by features. 
        #featureTS: Num of steps X dim of features. 
        self.dimFeats = featureTS.shape[1]; 
        self.bufferSym = list(featureTS); #buffer is initilized with transition sequences, as a list. 
        self.stackSym = [np.zeros(self.dimFeats)]; #list; needs to be converted to numpy array later. 
        self.bufferIndex = range(1,featureTS.shape[0]+1); #number of elements. start with 1, since 0 represents root. 

class Arc(object):
    '''
    the struct of arc, which is only for the goldArcs, 
    specifically, for each index in the trajectory, we will have a arc for it. 
    the arc of this index contains its head, and its dependents. 
    it could only have one head, but it could have multiple dependents; 
    '''
    def __init__(self):
        self.head = -1; #number,  -1 means there is no head
        self.dependents = []; #list

class arcHybirdKnownParser(object):
    '''
    implementation of archybirdknownparser. 
    '''
    def __init__(self, num_stack = 2, num_arcs = 2, num_buffer = 1, image = False):
        self.transitions = 'srl'; # Shift, Right, and Left. corresponds to 0,1,2
        self.selector = simpleFeatureSelector(
            num_stack = num_stack, num_arcs = num_arcs, num_buffer = num_buffer,
            image = image);
        
        self.featureTS = None;
        self.cnfg = None;

    def seed(self, num = 1000):
        pass;
    
    def reset(self, featureTS = None):
        self.featureTS = featureTS;
        self.cnfg = configuration();
        if self.featureTS is not None:
            self.cnfg.initilize_cnfg(featureTS);
            return self.selector.select(self.cnfg); #return observations. 

    def reset_all(self, featureTS):
        self.cnfgs = [];
        cnfg_obss = [];
        for i in xrange(len(featureTS)):
            cnfg = configuration();
            cnfg.initilize_cnfg(featureTS[i]);
            self.cnfgs.append(cnfg);
            cnfg_obss.append(self.selector.select(cnfg));
        return cnfg_obss;

    def legal_action(self, action, oldStep): 
        #oldStep is the current configuration. 
        #here we need to make sure that bufferSym and StackSym are always at least 2D. 
        #here we assume that the stack always have the ROOT at the end. 
        if action == 's' or action == 0: #action is shift:
            flag = (len(oldStep.bufferSym) >= 1); #the number of elements in the buffer should be at least 1. 
        elif action == 'r' or action == 1: #action is right:
            flag = (len(oldStep.stackSym) >= 2); #including the root in the stack.  At least two elements in the stack. Root is always at the stack. 
        elif action == 'l' or action == 2: #action is left:
            flag = ((len(oldStep.stackSym) >= 2) and (len(oldStep.bufferSym) >= 1)); #including root in the stack. 
            #we need to make sure that stack has two elements at least (root is always at stack), and buffer is not empty.
        else:
            print "Unknown transition for Arc-hybrid parser"; 
            embed()
            assert False;
        return flag;
    
    def check_done(self, cnfg):
        flag_s = self.legal_action(0, cnfg);
        flag_r = self.legal_action(1, cnfg);
        flag_l = self.legal_action(2, cnfg);
        if flag_s is False and flag_r is False and flag_l is False: #done
            return True; #means done.
        else:
            return False; #not done yet. 

    def end_cnfg(self, cnfg):
        if len(cnfg.bufferIndex) == 0 and len(cnfg.stackIndex) == 1:
            return True; #ended configuration. 
        else:
            return False;#not ended yet. 

    def step(self, action): 
        #checked.
        #this is the dynamics, f(c_t, a_t) => c_{t+1}
        #this implmentation corresponds to page 4: Arc-hybrid transition system
        #embed()
        if self.legal_action(action, oldStep = self.cnfg) is False:
            done = True;
            return [self.selector.select(self.cnfg), done];

        newStep = configuration();
        oldStep = self.cnfg;
        newStep.dimFeats = oldStep.dimFeats;
        if action == 's' or action == 0: #action is shift:
            label = 0.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym);
            newStep.stackSym.insert(0, oldStep.bufferSym[0]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym[1:]);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            #update index too, 
            newStep.stackIndex = copy.deepcopy([oldStep.bufferIndex[0]] + oldStep.stackIndex);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
        elif action == 'r' or action == 1: #action is right. 
            label = 1.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym[1:]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            newStep.arcsSym.insert(0, [oldStep.stackSym[1], oldStep.stackSym[0]]); 
            #update index, 
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.stackIndex[1], oldStep.stackIndex[0]]);
        elif action == 'l' or action == 2: #action is left:
            label = 2.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym[1:]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            newStep.arcsSym.insert(0, [oldStep.bufferSym[0], oldStep.stackSym[0]]);
            #update the index.
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.bufferIndex[0], oldStep.stackIndex[0]]);
        self.cnfg = copy.deepcopy(newStep);
        done = (self.check_done(self.cnfg) or self.end_cnfg(self.cnfg)); #either get to a state that no action is valid, or reachs the end. 
        obs = self.selector.select(self.cnfg);
        return [obs, done];





class Expert(object):
    #expert take the full configuration and the goldArcs and outputs a transition. 
    def __init__(self, parse = None):
        self.transitions = 'srl'; # Shift, Right, and Left. corresponds to 0,1,2
        self.parse = parse;

    def reset(self,parse):
        self.goldarcs = None;
        self.parse = parse;

    def getGoldArcs(self): #parse: ground truth: sequence of actions.
        assert self.parse is not None;
        parse = self.parse;
        arcs = [];
        maxInd = (len(parse) + 1)/2 + 1;  #this is because each arc has two elements. also need to add the extra root.  
        cnfg0 = configuration();
        cnfg0.bufferIndex = range(1,maxInd); #initlize the buffer index. 
        cnfg_t = cnfg0;
        for t in xrange(len(parse)):
            action = parse[t];
            cnfg_tp1 = self.updateSimple(action, cnfg_t)[-1];
            cnfg_t = cnfg_tp1;

        arcsIndex = cnfg_t.arcsIndex;
        for i in xrange(0, maxInd):
            arc_i = Arc();
            arcs.append(arc_i);

        for a in xrange(0, len(arcsIndex)):
            h = arcsIndex[a][0];
            d = arcsIndex[a][1];
            arcs[h].dependents.append(d);
            arcs[d].head = h; 
        self.goldarcs = arcs; 
        return self.goldarcs; 

    def legal_simple(self, action, oldStep):
        if action == 's' or action == 0:
            flag = (len(oldStep.bufferIndex) >= 1);
        elif action == 'r' or action == 1:
            flag = (len(oldStep.stackIndex) >= 2);
        elif action == 'l' or action == 2:
            flag = (len(oldStep.stackIndex) >= 2) and (len(oldStep.bufferIndex) >= 1);
        return flag;
    
        #only used for computing the gold arcs.
    def updateSimple(self, action, oldStep):
        #checked. 
        #this implmentation corresponds to page 4: Arc-hybrid transition system
        #for efficiency, here we only update the index, not the features, to save time. 
        newStep = configuration();
        newStep.dimFeats = oldStep.dimFeats;
        if action == 's' or action == 0:
            label = 0.;
            newStep.stackIndex = copy.deepcopy([oldStep.bufferIndex[0]] + oldStep.stackIndex);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
        elif action == 'r' or action == 1:
            label = 1.;
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.stackIndex[1], oldStep.stackIndex[0]]);
        elif action == 'l' or action == 2:
            label = 2.;
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.bufferIndex[0], oldStep.stackIndex[0]]);
        else:
            print "no such action"
            assert False
        return label, newStep;

    def cost(self, action, oldStep, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        if action == 'l' or action == 2:
            s0 = oldStep.stackIndex[0]; #take the first element in the stack, the rightmost of the stack. 
            s0_dependents = goldArcs[s0].dependents;
            num_s0_dep_in_buffer = len(list(set(s0_dependents) & set(oldStep.bufferIndex)));
            s0_head = [goldArcs[s0].head];
            num_s0_head_in_buffer = len(list(set(s0_head) & set(oldStep.bufferIndex[1:])));
            c = num_s0_head_in_buffer + num_s0_dep_in_buffer;
            if len(oldStep.stackIndex) >= 2: #should I consider the root here
                c = s0_head.count(oldStep.stackIndex[1]);

        elif action == 'r' or action == 1:
            s0 = oldStep.stackIndex[0]; 
            s0_head = [goldArcs[s0].head]; #this is just an element, so simply convert it to a list
            s0_dependents = goldArcs[s0].dependents;
            s0_head_dep = s0_head + s0_dependents;
            c = len(list(set(s0_head_dep) & set(oldStep.bufferIndex)));

        elif action == 's' or action == 0:
            b = oldStep.bufferIndex[0];
            b_head = [goldArcs[b].head];
            b_dep = goldArcs[b].dependents;
            c = len(list(set(b_head)&set(oldStep.stackIndex[1:]))) + len(list(set(b_dep)&set(oldStep.stackIndex)));
        else: 
            print "unknown action, exit";
            assert False;
        return c;
    
    def optimal(self, action, oldCnfg, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        #Tells whether the transition 'char' is optimal from the configuration oldCnfg.
        flag = self.legal_simple(action, oldCnfg);
        flag = (flag and (self.cost(action, oldCnfg, goldArcs) == 0));
        return flag;
    
    def sample_action(self, oldCnfg, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        #Finds an optimal next transition.
        actions = [];
        actions_index = [];
        for c in xrange(len(self.transitions)):
            if self.optimal(self.transitions[c], oldCnfg, goldArcs):
                actions.append(self.transitions[c]);
                actions_index.append(c);
        if len(actions) == 0: 
            print "no optimal action is found, check the code, it seems that this situation shouldn't happen"
        return actions_index[0];     
#################################################################################


class arcEagerKnownParser(object):
    '''
    implementation of archybirdknownparser. 
    '''
    def __init__(self, num_stack = 2, num_arcs = 2, num_buffer = 1, image = False):
        self.transitions = 'srld'; # Shift, Right, and Left. reduce, corresponds to 0,1,2.3
        self.selector = simpleFeatureSelector(
            num_stack = num_stack, num_arcs = num_arcs, num_buffer = num_buffer,
            image = image);
        
        self.featureTS = None;
        self.cnfg = None;

    def seed(self, num = 1000):
        pass;
    
    def reset(self, featureTS = None):
        self.featureTS = featureTS;
        self.cnfg = configuration();
        if self.featureTS is not None:
            self.cnfg.initilize_cnfg(featureTS);
            return self.selector.select(self.cnfg); #return observations. 
    
    def legal_action(self, action, oldStep): 
        if action == 's' or action == 0: #action is shift:
            flag = (len(oldStep.bufferSym) >= 1); #the number of elements in the buffer should be at least 1. 
        elif action == 'r' or action == 1: #action is right:
            flag = (len(oldStep.stackSym) >= 2) and (len(oldStep.bufferSym)>=1); 
            #including the root in the stack.  At least two elements in the stack. Root is always at the stack. 
        elif action == 'l' or action == 2: #action is left:
            flag = ((len(oldStep.stackSym) >= 2) and (len(oldStep.bufferSym) >= 1)); #including root in the stack. 
            flag=  flag and (len(set([oldStep.stackIndex[0]])&set(p[1] for p in oldStep.arcsIndex))==0);
        elif action == 'd' or action == 3:
            flag = (len(oldStep.stackSym) >=2);
            tmp_flag = (len(oldStep.bufferSym)==0) or (len(set([oldStep.stackIndex[0]])&set(p[1] for p in oldStep.arcsIndex))>0)
            flag = flag and tmp_flag 
        else:
            print "Unknown transition for Arc-hybrid parser"; 
            embed()
            assert False;
        return flag;
    
    def check_done(self, cnfg):
        flag_s = self.legal_action(0, cnfg);
        flag_r = self.legal_action(1, cnfg);
        flag_l = self.legal_action(2, cnfg);
        flag_d = self.legal_action(3, cnfg);
        if flag_s is False and flag_r is False and flag_l is False and flag_d is False: #done
            return True; #means done.
        else:
            return False; #not done yet. 

    def end_cnfg(self, cnfg):
        if len(cnfg.bufferIndex) == 0 and len(cnfg.stackIndex) == 1: #only root left
            return True; #ended configuration. 
        else:
            return False;#not ended yet. 

    def step(self, action): 
        if self.legal_action(action, oldStep = self.cnfg) is False:
            done = True;
            return [self.selector.select(self.cnfg), done];

        newStep = configuration();
        oldStep = self.cnfg;
        newStep.dimFeats = oldStep.dimFeats;
        if action == 's' or action == 0: #action is shift:
            label = 0.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym);
            newStep.stackSym.insert(0, oldStep.bufferSym[0]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym[1:]);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            #update index too, 
            newStep.stackIndex = copy.deepcopy([oldStep.bufferIndex[0]] + oldStep.stackIndex);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
        elif action == 'r' or action == 1: #action is right. 
            label = 1.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym);
            newStep.stackSym.insert(0, oldStep.bufferSym[0]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym[1:]);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            newStep.arcsSym.insert(0,[oldStep.stackSym[0],oldStep.bufferSym[0]]);
            
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex);
            newStep.stackIndex.insert(0,oldStep.bufferIndex[0]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.stackIndex[0],oldStep.bufferIndex[0]]);
        elif action == 'l' or action == 2: #action is left:
            label = 2.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym[1:]);
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            newStep.arcsSym.insert(0, [oldStep.bufferSym[0],oldStep.stackSym[0]]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.bufferIndex[0],oldStep.stackIndex[0]]);
        elif action =='d' or action == 3:
            label = 3.;
            newStep.stackSym = copy.deepcopy(oldStep.stackSym[1:]);
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferSym = copy.deepcopy(oldStep.bufferSym);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsSym = copy.deepcopy(oldStep.arcsSym);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);

        self.cnfg = copy.deepcopy(newStep);
        done = (self.check_done(self.cnfg) or self.end_cnfg(self.cnfg)); #either get to a state that no action is valid, or reachs the end. 
        obs = self.selector.select(self.cnfg);
        return [obs, done];


class Expert_Eager(object):

    def __init__(self, parse = None):
        self.transitions = 'srld'; # Shift, Right, and Left. corresponds to 0,1,2
        self.parse = parse;

    def reset(self,parse):
        self.goldarcs = None;
        self.parse = parse;

    def getGoldArcs(self): #parse: ground truth: sequence of actions.
        assert self.parse is not None;
        parse = self.parse;
        arcs = [];
        maxInd = (len(parse) + 1)/2 + 1;  #this is because each arc has two elements. also need to add the extra root.  
        cnfg0 = configuration();
        cnfg0.bufferIndex = range(1,maxInd); #initlize the buffer index. 
        cnfg_t = cnfg0;
        for t in xrange(len(parse)):
            action = parse[t];
            cnfg_tp1 = self.updateSimple(action, cnfg_t)[-1];
            cnfg_t = cnfg_tp1;

        arcsIndex = cnfg_t.arcsIndex;
        #embed()
        for i in xrange(0, maxInd):
            arc_i = Arc();
            arcs.append(arc_i);

        for a in xrange(0, len(arcsIndex)):
            h = arcsIndex[a][0];
            d = arcsIndex[a][1];
            arcs[h].dependents.append(d);
            arcs[d].head = h; 
        self.goldarcs = arcs; 
        return self.goldarcs; 


    def legal_simple(self, action, oldStep): 
        if action == 's' or action == 0: #action is shift:
            flag = (len(oldStep.bufferIndex) >= 1); #the number of elements in the buffer should be at least 1. 
        elif action == 'r' or action == 1: #action is right:
            flag = (len(oldStep.stackIndex) >= 2) and (len(oldStep.bufferIndex)>=1); 
            #including the root in the stack.  At least two elements in the stack. Root is always at the stack. 
        elif action == 'l' or action == 2: #action is left:
            flag = ((len(oldStep.stackIndex) >= 2) and (len(oldStep.bufferIndex) >= 1)); #including root in the stack. 
            flag=  flag and (len(set([oldStep.stackIndex[0]])&set(p[1] for p in oldStep.arcsIndex))==0);
        elif action == 'd' or action == 3:
            flag = (len(oldStep.stackIndex) >=2);
            tmp_flag = (len(oldStep.bufferIndex)==0) or (len(set([oldStep.stackIndex[0]])&set(p[1] for p in oldStep.arcsIndex))>0);
            flag = flag and tmp_flag 
        else:
            print "Unknown transition for Arc-eager parser"; 
            embed()
            assert False;
        return flag;

    def updateSimple(self, action, oldStep): 
        newStep = configuration();
        newStep.dimFeats = oldStep.dimFeats;
        if action == 's' or action == 0: #action is shift:
            label = 0.;
            newStep.stackIndex = copy.deepcopy([oldStep.bufferIndex[0]] + oldStep.stackIndex);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
        elif action == 'r' or action == 1: #action is right. 
            label = 1.;
            newStep.stackIndex = copy.deepcopy([oldStep.bufferIndex[0]]+oldStep.stackIndex);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex[1:]);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.stackIndex[0],oldStep.bufferIndex[0]]);
        elif action == 'l' or action == 2: #action is left:
            label = 2.;
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);
            newStep.arcsIndex.insert(0, [oldStep.bufferIndex[0],oldStep.stackIndex[0]]);
        elif action =='d' or action == 3:
            label = 3.;
            newStep.stackIndex = copy.deepcopy(oldStep.stackIndex[1:]);
            newStep.bufferIndex = copy.deepcopy(oldStep.bufferIndex);
            newStep.arcsIndex = copy.deepcopy(oldStep.arcsIndex);

        return label,newStep

    def cost(self, action, oldStep, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        if action == 'l' or action == 2:
            s0 = oldStep.stackIndex[0];
            s0_head = [goldArcs[s0].head];
            s0_dependents = goldArcs[s0].dependents;
            c = len(set(oldStep.bufferIndex[1:])&set(s0_dependents+s0_head));
     
        elif action == 'r' or action == 1:
            s0 = oldStep.bufferIndex[0]; 
            s0_head = [goldArcs[s0].head]; #this is just an element, so simply convert it to a list
            s0_dependents = goldArcs[s0].dependents;
            c_1 = len(set(s0_head)&set(oldStep.stackIndex[1:]+oldStep.bufferIndex[1:]));
            #nodes_have_head = [];
            #for i in xrange(0,len(oldStep.arcsIndex)):
            #    nodes_have_head = nodes_have_head + oldStep.arcsIndex[i];
            #nodes_have_head = set(nodes_have_head);
            nodes_have_head = set(p[1] for p in oldStep.arcsIndex);
            c_2 = len((set(s0_dependents)&set(oldStep.stackIndex[1:]))-nodes_have_head);
            c = c_1 + c_2;
        elif action == 'd' or action == 3:
            s = oldStep.stackIndex[0];
            c = len(set(oldStep.bufferIndex)&set(goldArcs[s].dependents));

        elif action == 's' or action == 0:
            b = oldStep.bufferIndex[0];
            c_1 = len(set([goldArcs[b].head])&set(oldStep.stackIndex));
            #nodes_have_head = [];
            #for i in xrange(0,len(oldStep.arcsIndex)):
            #    nodes_have_head = nodes_have_head + oldStep.arcsIndex[i];
            #nodes_have_head = set(nodes_have_head);
            nodes_have_head = set(p[1] for p in oldStep.arcsIndex);
            c_2 = len((set(goldArcs[b].dependents)&set(oldStep.stackIndex)) - nodes_have_head);
            c = c_1 + c_2;
        else: 
            print "unknown action, exit";
            assert False;
        return c;
    
    def optimal(self, action, oldCnfg, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        #Tells whether the transition 'char' is optimal from the configuration oldCnfg.
        flag = self.legal_simple(action, oldCnfg);
        flag = (flag and (self.cost(action, oldCnfg, goldArcs) == 0));
        return flag;
    
    def sample_action(self, oldCnfg, goldArcs = None):
        if goldArcs is None:
            goldArcs = self.goldarcs;
        assert goldArcs is not None;
        #Finds an optimal next transition.
        actions = [];
        actions_index = [];
        for c in xrange(len(self.transitions)):
            if self.optimal(self.transitions[c], oldCnfg, goldArcs):
                actions.append(self.transitions[c]);
                actions_index.append(c);
        if len(actions) == 0: 
            print "no optimal action is found, check the code, it seems that this situation shouldn't happen"
        return actions_index[0];     




#################################################################3
def compute_recall(predCnfg, goldArcs): #compute the recall
    #compute the recall of the given configuration, compared to the ground truth goldArcs. 
    arcsIndex = predCnfg.arcsIndex;
    common_arcs = 0.;
    for a in xrange(len(arcsIndex)):
        d = arcsIndex[a][1];
        h = arcsIndex[a][0];
        gold_h_of_d = goldArcs[d].head;
        if float(h) == float(gold_h_of_d):
            common_arcs = common_arcs + 1.;

    #recall = common_arcs / len(arcsIndex);
    recall = common_arcs / (len(goldArcs)-2);
    #print recall
    #embed()
    return recall;









if __name__ == '__main__':
    
    i = 100;
    [parses, featureTSs] = cPickle.load(open('position_based_data_eager.save'));	
    featureTSs = cPickle.load(open('hog_features_400.save'));
    N = len(parses);
    parser = arcEagerKnownParser();
    #parser = arcHybirdKnownParser();


    for i in xrange(len(parses)):
        parse = parses[i];
        exp = Expert_Eager(parse = parse);
        arcs = exp.getGoldArcs();
        print len(arcs)-featureTSs[i].shape[0];


    #print featureTSs[i].shape[0]
    #print len(arcs);




    

