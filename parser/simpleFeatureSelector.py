import numpy as np 
#from arcHybirdKnownParser import * 
#from parser import configuration, Arc, arcHybirdKnownParser
from IPython import embed

class simpleFeatureSelector(object):
	'''
	in Defualt:
	This class selects the 5 top stack entries, 5 most recent dependency
    arcs, and front element of the buffer, and concatenates their
    features.
    '''

	def __init__(self, num_stack = 2, num_arcs = 2, num_buffer = 1, image = False): #defaul: top 5 elem from stack, top 5 elem from arcs, top 1 from buffer. 
		self.num_stack = num_stack;
		self.num_arcs =num_arcs;
		self.num_buffer = num_buffer;
		self.image = image;

	def select(self, cnfg):  #extract features from the configuration 
		stack = cnfg.stackSym;  #a list. 
		feadim = cnfg.dimFeats;  
		stacksize = len(cnfg.stackSym);
		stackFeatures = np.zeros((self.num_stack, feadim)); 
		if stacksize >= self.num_stack: 
			stackFeatures = np.array(cnfg.stackSym[0:self.num_stack]);
		else:
			stackFeatures[0:stacksize] = np.array(cnfg.stackSym);

		arcsFeatures = np.zeros((self.num_arcs, feadim*2));
		arcsSize = len(cnfg.arcsSym); 
		if arcsSize >= self.num_arcs:
			tmpmatrix = np.array(cnfg.arcsSym[0:self.num_arcs]);
			#embed()
			arcsFeatures = tmpmatrix.reshape(tmpmatrix.shape[0], tmpmatrix.shape[1]*tmpmatrix.shape[2]);
		elif arcsSize != 0: 
			tmpmatrix = np.array(cnfg.arcsSym); 
			arcsFeatures[0:arcsSize] = tmpmatrix.reshape(tmpmatrix.shape[0], tmpmatrix.shape[1]*tmpmatrix.shape[2]);

		bufferFeatures = np.zeros((self.num_buffer, feadim));
		bufferSize = len(cnfg.bufferSym);
		if bufferSize >= self.num_buffer: 
			bufferFeatures = np.array(cnfg.bufferSym[0:self.num_buffer]);
		elif bufferSize != 0:
			bufferFeatures[0:bufferSize] = np.array(cnfg.bufferSym);

		if self.image == True:
			selectedFeatures = np.zeros((self.num_stack+self.num_arcs*2+self.num_buffer, 40,40));
			for i in xrange(0, self.num_stack):
				selectedFeatures[i] = stackFeatures[i].reshape(40,40).T;
			for i in xrange(0, self.num_buffer):
				selectedFeatures[self.num_stack+i] = bufferFeatures[i].reshape(40,40).T;
			for i in xrange(0, self.num_arcs):
				selectedFeatures[self.num_stack+self.num_buffer + 2*i] = arcsFeatures[i][0:feadim].reshape(40,40).T;
				selectedFeatures[self.num_stack+self.num_buffer + 2*i+1]=arcsFeatures[i][feadim:].reshape(40,40).T;
		else:
			selectedFeatures = np.hstack((stackFeatures.ravel(), arcsFeatures.ravel(), bufferFeatures.ravel()));
	   	return selectedFeatures;


if __name__ == '__main__':

	selector = simpleFeatureSelector(num_stack = 2, num_arcs = 2, num_buffer = 2);
	cnfg = configuration();
	cnfg.initilize_cnfg(np.random.rand(5,2));
	#cnfg.arcsSym.append([np.random.rand(2),np.random.rand(2)]);
	#cnfg.bufferSym = [];
	f = selector.select(cnfg);






    		