import numpy as np
from IPython import embed
import gym
from network_set import Policy_discrete
from aggrevated_set import AggreVate_discrete
#from policy_discrete import Policy_discrete
#from AggreVate import AggreVate_set
#from AggreVate_Discrete import AggreVate_discrete
import sys


if __name__ == '__main__':
    np.random.seed(1000);
    robot_name = str(sys.argv[1]);
    imitation_train_method = str(sys.argv[2]);
    policy_train_method = 'explicit GD L2'  #str(sys.argv[2]);
    
    if robot_name == 'Acrobot':
        openAI_model_name = 'Acrobot-v1';
    elif robot_name == 'CartPole':
        openAI_model_name = 'CartPole-v0';
    else:
        print "doesn't support the robot named {}".format(robot_name);

    #openAI_model_name = 'Acrobot-v1';
    #openAI_model_name = 'MountainCar-v0';
    #openAI_model_name = 'CartPole-v0';
    env = gym.make(openAI_model_name);
    env.seed(1000)
    obs_dim = env.reset().shape[0];
    x_dim = env.state.shape[0];
    
    a_dim = 1;
    num_actions = env.action_space.n;

    if openAI_model_name == 'Acrobot-v1':
        T = 200;
        im_lr = 0.2;
        KL_Delta = 0.01;
        Q_star_name = 'acrobot_keras_model_Q_star_vector.h';
    elif openAI_model_name == 'CartPole-v0':
        T = 500;
        Q_star_name = 'cartpole_keras_model_Q_star_vector.h'
        im_lr = 0.1;
        KL_Delta = 0.02;

    num_roll_in = 100;
    po_or_im = 'im'  #str(sys.argv[3]);

    params = {'num roll in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[16], 
    'evn':openAI_model_name, 'expert':Q_star_name, 
    'n_epoch':200, 'im_lr':im_lr,'po_lr':1e-1*2.,'KL Delta':KL_Delta, 
    'Agg_iter':20, 'Pre-Train':False,
    'filter':False, 'vr':True, 'reg':0.0, 'cg_iter':20};    
    print params;

    final_results = [];
    for repeat in xrange(10):
        env = gym.make(openAI_model_name);
        env.seed(repeat);
        np.random.seed(repeat);
        print "at repeat {}".format(repeat);
        pi = Policy_discrete(dx = obs_dim, k = num_actions, num_layers = params['num hid Layer'], 
                            nh = params['nh'], 
                            num_roll_in = params['num roll in'], cg_iter = params['cg_iter'], reg = params['reg']);
        Learner = AggreVate_discrete(env = env, policy_model = pi, obs_dim = obs_dim, 
                                x_dim = x_dim, a_dim = a_dim, 
                                num_actions = num_actions, T = params['T'], 
                                num_roll_in = params['num roll in'], 
                                imitation_train_method = params['imitation'],
                                policy_train_method = params['policy'], 
                                setup_filter = params['filter'], vr = params['vr']);
    
        Learner.load_saved_Q_star(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 50, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);
    
        if po_or_im == 'im':
            print "perform imitation"
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                            pre_train = params['Pre-Train'], 
                            n_epoch = params['n_epoch'], lr = params['im_lr'], 
                            KL_Delta = params['KL Delta'], 
                            MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'im':im_rr}#, 'po':po_rr};
        elif po_or_im == 'rl':
            print "perform RL"
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'po':po_rr};

        final_results.append(exp_im_po);

    filername = 'result_Model_{}_numrollin_{}_T_{}_method_{}_expert_{}_n_epoch_{}_imlr_{}_polr_{}_KLD_{}_PT_{}_F_{}'.format(
            params['evn'],params['num roll in'],params['T'],params['imitation'], params['expert'],
            params['n_epoch'],params['im_lr'], params['po_lr'],params['KL Delta'], 
            params['Pre-Train'], params['filter']);
