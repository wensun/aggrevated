import numpy as np
from IPython import embed
import gym
from network_set import LSTM_based_Policy
#from single_RNN_policy import LSTM_based_Policy
from aggrevated_set import partial_environment
from aggrevated_set import AggreVate_Discrete_Partial
#from AggreVate_Discrete_Partial import AggreVate_Discrete_Partial
#from AggreVate_Discrete_Partial import partial_environment
import sys

if __name__ == '__main__':

    np.random.seed(1000);
    #openAI_model_name = 'Acrobot-v1';
    robot_name = str(sys.argv[1]);
    #openAI_model_name = str(sys.argv[1]);
    if robot_name == 'Acrobot':
        openAI_model_name = 'Acrobot-v1';
    else:
        print "current doesn't support the robot named {}".format(robot_name);
        assert False;

    obs_index = [0,2] if openAI_model_name == 'CartPole-v0' else [0,1,2,3]; #first 4 elements for Acrobot.
    env = partial_environment(openAI_model_name, obs_index = obs_index);
    env.seed(1000)
    obs_dim = len(obs_index);
    x_dim = env.state.shape[0];
    
    a_dim = 1;
    num_actions = env.n_actions;
    if openAI_model_name == 'Acrobot-v1':
        T = 200;
        Q_star_name = 'acrobot_keras_model_Q_star_vector.h';
    elif openAI_model_name == 'CartPole-v0':
        T = 500;
        Q_star_name = 'cartpole_keras_model_Q_star_vector.h'

    num_roll_in = 100;
    imitation_train_method = str(sys.argv[2]);
    policy_train_method =  'explicit GD L2' # str(sys.argv[3]);
    po_or_im = 'im'

    params = {'num_roll_in':num_roll_in, 'T':T, 
    'imitation':imitation_train_method, 'policy':policy_train_method, 
    'num hid Layer':1, 'nh':[16], 
    'evn':openAI_model_name, 'expert':Q_star_name, 
    'n_epoch':200, 'im_lr':1e-2*1.,'po_lr':1e-3,
    'KL Delta':1e-3*2., 'Agg_iter':50, 
    'Pre-Train':False,
    'filter':False, 'vr':True, 'reg':0.0, 'cg_iter':20};    
    print params;

    final_results = [];

    for repeat in xrange(5):
        env = partial_environment(openAI_model_name, obs_index = obs_index);
        env.seed(repeat);
        np.random.seed(repeat);
        print "at repeat {}".format(repeat);
        
        Pi = LSTM_based_Policy(x_dim = obs_dim, a_dim = a_dim, output_dim = num_actions,
            nh = 16, num_roll_in = params['num_roll_in'], cg_iter= params['cg_iter'],
            reg = params['reg'], discrete = True);

        Learner = AggreVate_Discrete_Partial(env = env, policy_model = Pi, 
                obs_dim = len(obs_index), x_dim = x_dim, a_dim = a_dim, num_actions = num_actions,
                T = params['T'], num_roll_in = params['num_roll_in'], 
                imitation_train_method = params['imitation'], 
                policy_train_method = params['policy'], 
                setup_filter = params['filter'], vr = params['vr']);
        
        Learner.load_saved_Q_star(params['expert']);
        expert_info = Learner.test_policy(num_test_roll_in = 50, expert = True);
        print "Expert cost mean {} and std {}".format(expert_info[0], expert_info[1]);
    
        #embed()
        if po_or_im == 'im':
            print "perform imitation"
            im_rr = Learner.AggreVate_imitation(total_iter = params['Agg_iter'], 
                            pre_train = params['Pre-Train'], 
                            n_epoch = params['n_epoch'], lr = params['im_lr'], 
                            KL_Delta = params['KL Delta'], 
                            MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'im':im_rr}#, 'po':po_rr};
        
        elif po_or_im == 'rl':   
            print "perform RL"
            po_rr = Learner.Policy_gradient(total_iter = params['Agg_iter'],
                        n_epoch = params['n_epoch'], lr = params['po_lr'],
                        KL_Delta = params['KL Delta'], MC = False);
            exp_im_po = {'expert':(expert_info[0], expert_info[1]), 'po':po_rr};

        final_results.append(exp_im_po);