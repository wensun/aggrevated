import numpy as np
from IPython import embed
import gym
from network_set import LSTM_Policy
from aggrevated_set import AggreVate_parsing
import copy
from scipy import stats
import sys
from parser import *
import cPickle




if __name__ == '__main__':
    np.random.seed(1000);
    system = str(sys.argv[1]);
    imitation_train_method = str(sys.argv[2]);
    policy_train_method = 'explicit GD L2' #str(sys.argv[3]);
    
    feature_name = './parsing_data/hog_features_pca_50_400.save';
    [parses, featureTSs] = cPickle.load(open('./parsing_data/position_based_data_{}.save'.format(system)));	
    featureTSs = cPickle.load(open(feature_name));
    N = len(parses);
    parses = np.array(parses);
    featureTSs = np.array(featureTSs);

    pert = np.random.choice(len(featureTSs), len(featureTSs),replace = False);
    featureTSs_train = featureTSs[pert][:int(N*0.8)];
    parses_train = parses[pert][:int(N*0.8)];
    featureTSs_valid = featureTSs[pert][int(N*0.8):int(N*0.9)];
    parses_valid = parses[pert][int(N*0.8):int(N*0.9)];
    featureTSs_test = featureTSs[pert][int(0.9*N):];
    parses_test = parses[pert][int(N*0.9):];
    
    T = 100;
    num_stack = 1;
    num_arcs = 1;
    num_buffer = 1;
    image = False
    num_roll_in = 30;
    selector = simpleFeatureSelector(num_stack = num_stack,num_arcs=num_arcs, 
                                num_buffer = num_buffer, image = image);
    
    if system == 'hybird':
        env = arcHybirdKnownParser(num_stack=num_stack,num_arcs=num_arcs,
                                num_buffer=num_buffer,image=image);
        Expert = Expert();
    elif system == 'eager':
        env = arcEagerKnownParser(num_stack = num_stack,num_arcs=num_arcs,
                                num_buffer=num_buffer,image=image);
        Expert = Expert_Eager();
    
    num_layers = 1
    nh = 50;
    ridge = -1.;
    pre_train = True;
    real_testing = True;
    n_epoch = 10;
    lr = 1e-2*1.;
    RL_lr = 1e-2;
    KL_Delta = 1e-2*1.;
    RL_KL_Delta = 1e-4*1.;
    total_iter = 80;
    alpha = 1.0 #0.1;
    natural_reg = 0#1e-3;#1e-3;
    cg_iter = 10;
    vr = True;
    im_or_rl = 'im'
    tanh = False;
    
    params = {'system':system,'feature_file_name': feature_name, 'T':T, 
            'num_stack':num_stack, 'num_arcs':num_arcs, 'num_buffer':num_buffer,
            'image':image, 'num_roll_in':num_roll_in, 'num_layers':num_layers, 'nh':nh,
            'ridge':ridge, 'imitation_train_method':imitation_train_method,
            'policy_train_method':policy_train_method, 'pre_train':pre_train, 'n_epoch':n_epoch,
            'lr':lr, 'RL_lr':RL_lr, 'KL_Delta':KL_Delta, 'RL_KL_Delta':RL_KL_Delta, 
            'total_iter':total_iter, 'alpha':alpha,
            'cg_iter': cg_iter, 'natural_reg':natural_reg,
            'vr':vr, 'im_or_rl':im_or_rl,  'tanh':tanh};

    print params;
    tmp_obs = env.reset(featureTSs[0]);
    x_dim_1 = featureTSs[0].shape[1];
    x_dim_2 = tmp_obs.shape[0];
    num_actions = len(env.transitions);

    final_results = [];
    if real_testing:
        featureTSs_try = featureTSs_test;
        parses_try = parses_test;
    elif real_testing is False:
        featureTSs_try = featureTSs_valid;
        parses_try = parses_valid;

    pi = LSTM_Policy(x_dim_1 = x_dim_1, x_dim_2 = x_dim_2, a_dim = num_actions, output_dim = num_actions,
                        nh = params['nh'], num_roll_in = params['num_roll_in'], ridge = params['ridge'],
                        cg_iter = params['cg_iter'], reg = params['natural_reg']);

    Learner = AggreVate_parsing(env=env, policy_model = pi, expert = Expert,
                feature_selector = selector, obs_dim = x_dim_2, x_dim= x_dim_1,
                a_dim = 1,num_actions =num_actions, T = T, 
                imitation_train_method = params['imitation_train_method'],
                policy_train_method = params['policy_train_method'], params = params, 
                vr = params['vr'], testing = real_testing, im_reinforce = False);
    
    expert_perf = Learner.test_policy(featureTSs, parses, expert = True);
    print "expert performance: mean {} and std {}".format(expert_perf[0],expert_perf[1]);

    if im_or_rl == 'im':
        results_im = Learner.AggreVate_imitation(featureTSs_train = featureTSs_train, 
                    parses_train = parses_train, featureTSs_valid = featureTSs_try, 
                    parses_valid = parses_try, total_iter = params['total_iter'], 
                    batch_size=num_roll_in, pre_train=params['pre_train'],
                    n_epoch = params['n_epoch'], lr = params['lr'], KL_Delta = params['KL_Delta'],
                    alpha = params['alpha']);
        exp_im = {'expert':(expert_perf[0],expert_perf[1]), 'im':results_im}; 
    else:
        results_po = Learner.Policy_gradient(featureTSs_train=featureTSs_train, 
                    parses_train = parses_train, featureTSs_valid=featureTSs_try, 
                    parses_valid=parses_try, total_iter = params['total_iter'], 
                    batch_size = num_roll_in, n_epoch = params['n_epoch'],
                    lr = params['RL_lr'], KL_Delta = params['RL_KL_Delta'], pre_train = params['pre_train']);
        exp_im = {'expert':(expert_perf[0],expert_perf[1]), 'po':results_po}; 
    
    final_results.append(exp_im);